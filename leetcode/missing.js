/*
Given an unsorted integer array, find the first missing positive integer.
For example,
Given [1,2,0] return 3,
and [3,4,-1,1] return 2.
Your algorithm should run in O(n) time and uses constant space.
*/

function findMissing(input) {
  for(let i = 0; i < input.length; i++){
    if(input[i] >= 0) {
      let result = findNext(input[i], input.slice(1));
      if(result != null) {
        return result;
      }
    }
  }
}

function findNext(num, input) {
  if(input.length === 0){
    return null;
  }
  if(input[0] !== num+1 && input[0]-1 !== num){
    return findNext(input[0], input.slice(1)); 
  }
  return input[0];
}

console.log(findMissing([1,2,0]));
console.log(findMissing([3,4,-1,1]));