/*
Example 1:

Input:
s = "catsanddog"
wordDict = ["cat", "cats", "and", "sand", "dog"]
Output:
[
  "cats and dog",
  "cat sand dog"
]

Example 2:

Input:
s = "pineapplepenapple"
wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
Output:
[
  "pine apple pen apple",
  "pineapple pen apple",
  "pine applepen apple"
]
Explanation: Note that you are allowed to reuse a dictionary word.

Example 3:

Input:
s = "catsandog"
wordDict = ["cats", "dog", "sand", "and", "cat"]
Output:
[]

*/


var wordBreak = function(s, wordDict) {
    let wordDictMap = {};
    for(let i = 0; i < wordDict.length; i++){
        wordDictMap[wordDict[i]] = true;
    }
    let results = [];
    for(let i = 0; i < s.length; i++){
        let spacesStr = '';
        for(let e = i+1; e < s.length; e++) {
            let word = s.substring(i, e);
            if(wordDictMap[word]) {
               spacesStr = word + ' ';
               i = e;
            }
        }
        results.push(spacesStr);
    }
    return results;
};

var s = "catsanddog"
var wordDict = ["cat", "cats", "and", "sand", "dog"]
console.log(wordBreak(s, wordDict));

