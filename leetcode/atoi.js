var myAtoi = function(str) {
  if(!str){
  //throw new Error('Invalid arg');
    return 0;
  }
  const nums = {
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9
  };
  let isNeg = false;
  let startIndex = 0;
  if(str[0] === '-') {
    isNeg = true;
    startIndex = 1;
  }
  //console.log(str, isNeg, startIndex);
  let total = 0;
  for(let i = startIndex; i < str.length; i++) {
    let num = nums[str[i]];
    let multiplerLength = str.length - i - 1;
    //console.log('-', multiplerLength);
    let baseNum = Math.pow(10, multiplerLength);
    baseNum *= num;
    total += baseNum;
    //console.log('--', total);
  }
  return isNeg ? total*-1 : total;
};

//console.log(myAtoi(''));
//console.log(myAtoi(null));

console.log(myAtoi('0'));
console.log(myAtoi('1'));
console.log(myAtoi('10'));
console.log(myAtoi('100'));
console.log(myAtoi('9999'));
console.log(myAtoi('-0'));
console.log(myAtoi('-9999'));
