
var myPow = function(x, n) {
  let result = 1;
    for(let i = 0; i < Math.abs(n); i++){
      result *= x;
    }
  if(n < 0) {
    result = 1/result;
  }
  return result;
};

console.log(myPow(2.00000, -2147483647));
