/*
https://leetcode.com/problems/regular-expression-matching/description/

'.' Matches any single character.
'*' Matches zero or more of the preceding element.

The matching should cover the entire input string (not partial).

The function prototype should be:
bool isMatch(const char *s, const char *p)

Some examples:
isMatch("aa","a") → false
isMatch("aa","aa") → true
isMatch("aaa","aa") → false
isMatch("aa", "a*") → true
isMatch("aa", ".*") → true
isMatch("ab", ".*") → true
isMatch("aab", "c*a*b") → true
*/

console.log(isMatch('aa', 'a'));
console.log(isMatch('aa', 'aa'));
console.log(isMatch('aaa', 'aa'));
console.log(isMatch('aaa', 'a*'));

function isMatch(input, match) {
  let preceding = null;
  let inputIdx = 0;
  let matchIdx = 0;
  while(true) {
    if(matchIdx >= match.length && inputIdx < input.length) {
      return false;
    }
    if(matchIdx === match.length && inputIdx === input.length) {
      return true;
    }
    if(match[matchIdx] === '.'){
      inputIdx++;
      matchIdx++;
      continue;
    } else if (match[matchIdx] === '*') {
      preceding = input[inputIdx-1];
      inputIdx++;
      // matchIdx++;
    } else {
      if(preceding) {
        //get the next match character, if none, return true
        // else match the input string till the next match character
      } else {
        if (match[matchIdx+1] === '*') {

        } else {
          if(match[matchIdx] === input[inputIdx]) {
            inputIdx++;
            matchIdx++;
          } else {
            return false;
          }
        }
      }
    }
  }
  return true;
}