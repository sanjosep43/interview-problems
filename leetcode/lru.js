
class LRU {
  constructor(capacity) {
    this.capacity = capacity;
    this.data = {};
    this.used = {};
  }

  get(key) {
    if(!this.data[key]){
      return -1;
    }
    if(!this.used[key]) {
      this.used[key] = 1;
    } else {
      this.used[key]++;
    }
    return this.data[key];
  }

  put(key, value){
    if(this.data[key]){
      this.used[key]++;
      this.data[key] = value;
      return;
    }
    if( Object.keys(this.data).length === this.capacity ){
      let keys = Object.keys(this.data);
      let leastKey = 100;
      for(let i = 0; i < keys.length; i++){
        if(this.used[keys[i]] < leastKey) {
          leastKey = keys[i];
        }
      }
      delete this.data[leastKey];
    }
    this.data[key] = value;
  }
}


let cache = new LRU(2);
cache.put(1, 1);
cache.put(2, 2);
console.log(cache.get(1));       // returns 1
cache.put(3, 3);    // evicts key 2
console.log(cache.get(2));       // returns -1 (not found)
cache.put(4, 4);    // evicts key 1
console.log(cache.get(1));       // returns -1 (not found)
console.log(cache.get(3));       // returns 3
console.log(cache.get(4));       // returns 4