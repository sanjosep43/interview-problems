
var isNumber = function(s) {
    //trim blanks from front & end
    s = s.trim();
    if(s.indexOf(' ') > -1) {
        return false;
    }
    const int = parseInt(s, 10);
    if(isNaN(int)) {
        const float = parseFloat(s, 10);
        if(isNaN(float)) {
            return false;
        }
        return true;
    }
    return true;
}
