function countSmaller (nums) {
  let results = [];
  for(let i = 0; i < nums.length; i++){
    let current = nums[i];
    let count = 0;
    for(let x = i + 1; x < nums.length; x++){
      if(nums[x] < current) {
        count++;
      }
    }
    results.push(count);
  }
  return results;
};

console.log(countSmaller([5, 2, 6, 1]));