var firstMissingPositive = function(nums) {
  let smallestInt = 1;
  for(let i = 0; i < nums.length; i++) {
    if(nums[i] <= smallestInt) {
      smallestInt = nums[i] + 1;
    }
  }
  return smallestInt;
};

function findSmallestFrom(nums, from) {
    let smallest = from;
    for(let i = 0; i < nums.length; i++) {
        if(nums[i] < 0) {
            continue;
        }
        if(nums[i] < from) {
           smallest = nums[i] - 1;
        }
    }
    let exist = false;
    for(let i = 0; i < nums.length; i++) {
        if(nums[i] === smallest) {
            exist = true;
            break;
        }
    }
    if(exist || smallest === 0) {
        smallest = findSmallestFrom(nums, from+1);
    }
    return smallest;
}

console.log(findSmallestFrom([1,2,0], 1));  //3
console.log(findSmallestFrom([3,4,-1,1], 1)); //2
console.log(findSmallestFrom([7,8,9,11,12], 1)); //1
//console.log(firstMissingPositive([1,2,0]));
