/**
 * @param {number} left
 * @param {number} right
 * @return {number[]}
 */
var selfDividingNumbers = function(left, right) {
  let results = [];
  for(let i = left; i <= right; i++) {
    let digits = [];
    let hasDigits = true;
    let num = i;
    while(hasDigits){
        let digit = parseInt(num % 10);
        digits.push(digit);
        num = parseInt(num / 10);
        if( num === 0 ) {
            hasDigits = false;
        }
    }
    let selfDividing = true;
    digits.forEach(d => {
        if(i % d !== 0) {
            selfDividing = false;
        }
    });
    if(selfDividing) {
        results.push(i);
    }
  }
  return results;
};

console.log(selfDividingNumbers(1, 22));