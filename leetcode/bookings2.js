var MyCalendarTwo = function() {
  this.days = {};
};

/** 
* @param {number} start 
* @param {number} end
* @return {boolean}
*/
MyCalendarTwo.prototype.book = function(start, end) {
  // console.log(start, end);
  for(let i = start; i <= end; i++){
    // console.log(i);
    if(!this.days[i]){
      // console.log('1')
        this.days[i] = 1;
    } else if (this.days[i] === 1) {
      // console.log('2')
        this.days[i] = 2;
    } else {
      return false;
    }
  }
  // console.log('true:', this.days);
  return true; 
};

// [[],[10,20],[50,60],[10,40],[5,15],[5,10],[25,55]]
let cal = new MyCalendarTwo();
console.log(cal.book(10, 20)); //t
console.log(cal.book(50, 600)); //t
console.log(cal.book(10, 40)); //t
console.log(cal.book(5, 15)); //f
console.log(cal.book(5, 10)); //t
//console.log(cal.book(25, 55)); //t