var MyCalendar = function() {
  this.days = {};
  this.segments = [];
};

MyCalendar.prototype.book = function(start, end) {
  for(let i = 0; i < this.segments.length; i++){
    if(start >= this.segments[i].start && start <= this.segments[i].end) {
      return false;
    }
    if(end-1 >= this.segments[i].start && end-1 <= this.segments[i].end) {
      return false;
    }
  }
  this.segments.push({start: start, end: end-1});
  return true;
};

MyCalendar.prototype.book2 = function(start, end) {
  for(let i = start; i < end; i++){
    if(this.days[i]){
      // console.log('false at:',i, this.days);
      return false;
    }
  }
  for(let i = start; i < end; i++){
    // console.log(i);
   this.days[i] = true;
  }
  // console.log('true:', this.days)
  return true;
};


let cal = new MyCalendar();
// console.log(cal.book(10, 20)); //t
// console.log(cal.book(15, 25)); //f
// console.log(cal.book(20, 30)); //t

//[true,true,false,false,true,false,true,true,true,false]
console.log(cal.book(47, 50)); //t
console.log(cal.book(33, 41)); //t
console.log(cal.book(39, 45)); //f
console.log(cal.book(33, 42)); //f
console.log(cal.book(25, 32)); //t
console.log(cal.book(26, 35)); //f
console.log(cal.book(19, 25)); //t - problem
console.log(cal.book(3,  8));  //t
console.log(cal.book(8,  13)); //t
console.log(cal.book(18, 27)); //f

/*
[[],[20,29],[13,22],[44,50],[1,7],[2,10],[14,20],[19,25],[36,42],[45,50],[47,50],[39,45],[44,50],[16,25],[45,50],[45,50],[12,20],[21,29],[11,20],[12,17],[34,40],[10,18],[38,44],[23,32],[38,44],[15,20],[27,33],[34,42],[44,50],[35,40],[24,31]]
*/