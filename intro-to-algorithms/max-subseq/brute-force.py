from math import inf

input = [0, -9, 3, 2, -8, -6, 8, 7, 3]
max = -inf
current = 0
maxi = -1
maxj = -1
len = len(input)
for i in range(0, len):
    for j in range(i, len):
        current = sum(input[i:j])
        if current > max:
            max = current
            maxi = i
            maxj = j

print("total:", max, "i:", maxi, "j:", maxj)
