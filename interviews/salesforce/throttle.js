
// we don't allow to our function to execute more than once every X milliseconds.
function throttle(fn, time) {
  return function() {
    setTimeout(fn, time);
  }
}

const fn1 = throttle(() => console.log('1'), 500);

fn1();
fn1();
fn1();