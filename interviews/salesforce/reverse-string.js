

function reverse(str) {
  let s = '';
  if(!str){
    return s;
  }
  for(let i = str.length-1; i >= 0; i--){
    s += str[i];
  }
  return s;
}

String.prototype.reverse = function() {
  let s = '';
  for(let i = this.length-1; i >= 0; i--){
    s += this[i];
  }
  return s;
};

console.log(reverse('abcdef'));
console.log('abc'.reverse());
console.log('a'.reverse());
console.log(''.reverse());
console.log('abcdefghi'.reverse());
