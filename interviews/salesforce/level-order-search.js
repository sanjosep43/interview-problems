const createTree = require('../../techie-delight/binary-tree/create-binary-tree');

const t1 = createTree([10,2,3, 30, 40, 50, 8, 4]);

console.log(JSON.stringify(t1));

function levelOrderTraversel(node, cb) {
  const q = [];
  q.push(node);
  while(q.length > 0) {
    const n = q.pop();
    cb(n.val);
    if(n.left) {
      q.push(n.left);
    }
    if(n.right){
      q.push(n.right);
    }
  }
}

levelOrderTraversel(t1, console.log);