
class Stack {
  constructor() {
    this.queue = []; //push, shift(pop_front)
  }
  push(x) {
    this.queue.push(x);
  }
  pop() {
    const q2 = [];
    while(this.queue.length > 1 ) {
      q2.push(this.queue.shift());
    }
    const item = this.queue.shift();
    while(q2.length > 0) {
      this.queue.push(q2.shift());
    }
    return item;
  }
  top() {

  }
}

const stack = new Stack();
stack.push(1);
stack.push(2);
stack.push(3);
console.log(stack.pop());
console.log(stack.pop());
console.log(stack.pop());