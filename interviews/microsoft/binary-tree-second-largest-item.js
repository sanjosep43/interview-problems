
function secondLargestItem(node) {
  const result = [];
  helper(node, result);
  return result[0];
}

function helper(node, result) {
  if(!node) {
    return;
  }
  if(result.length === 0){
    result[1] = node.val;
  } else if( !result[0] ) {
    if(node.val > result[1]){
      result[0] = result[1];
      result[1] = node.val;
    } else {
      result[0] = node.val;
    }
  } else {
    if(node.val >= result[1]) {
      result[0] = result[1];
      result[1] = node.val;
    } else if( node.val < result[1] && node.val >= result[0]) {
      result[0] = node.val;
    }
  }
  helper(node.left, result);
  helper(node.right, result);
}

exports.secondLargestItem = secondLargestItem;