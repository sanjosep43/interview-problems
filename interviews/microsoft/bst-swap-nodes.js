
function swapBadNodes(node) {
  helper(node, node);
}

function helper(root, node) {
  if(!node){
    return;
  }
  if(node.left) {
    if(node.left.val >= node.val) {
      return helper2(root, node.left, node, 'left');
    }
  }
  if(node.right) {
    if(node.right.val < node.val) {
      return helper2(root, node.right, node, 'right');
    }
  }
  helper(root, node.left);
  helper(root, node.right);
}

function helper2(node, badNode, badNodeParent, side) {
  if(!node) {
    return;
  }
  if(node.left) {
    if(node !== badNode && node.left.val >= node.val) {
      //replace node.left with badNode and then node.left children need to point to badNode children
      const leftTemp = node.left.left;
      const rightTemp = node.left.right;
      node.left = badNode;
      badNode.left = node.left.left;
      badNode.right = node.left.right;
      node.left.left = leftTemp;
      node.left.right = rightTemp;
    }
  }
  if(node.right) {
    if(node !== badNode && node.right.val >= node.val) {
    }
  }
  helper2(node.left, badNode, badNodeParent, side);
  helper2(node.right, badNode, badNodeParent, side);
}
