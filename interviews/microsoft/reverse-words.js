
function reverseWord(str) {
  let s = '';
  for(let i = str.length - 1; i >= 0; i--){
    s += str[i];
  }
  return s;
}

function reverseWords(str) {
  if(!str) {
    return str;
  }
  if(str.indexOf(' ') === -1) {
    return reverseWord(str);
  }
  let result = '';
  for(let i = 0; i < str.length; i++) {
    let spaceIdx = str.indexOf(' ', i);
    if(spaceIdx === -1){
      // console.log('--', i, str.substring(i))
      result += reverseWord(str.substring(i))
      break;
    } else {
      // console.log('-', i, spaceIdx, str.substring(i, spaceIdx))
      const reversedWord = reverseWord(str.substring(i, spaceIdx));
      result += reversedWord + ' ';
    }
    i = spaceIdx;
  }
  return result;
}

exports.reverseWords = reverseWords;