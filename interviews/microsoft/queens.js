
function NQueens(boardSize, numQueens) {
  const board = [];
  for(let r = 0; r < boardSize; r++) {
    let row = [];
    for(let c = 0; c < boardSize; c++) {
      row.push(0);
    }
    board.push(row);
  }
  const points = [];
  let currentPointIdx = 0;
  for(let r = 0; r < boardSize; r++) {
    for(let c = 0; c < boardSize; c++) {
      points.push({r, c});
      if(checkRow(board[r]) || checkColumn(board, c) || checkDiagonal(board, boardSize, r, c)) {
        points.pop();
        currentPointIdx = currentPointIdx - 1;
        r = points[currentPointIdx].r;
        c = points[currentPointIdx].c + 1;
        continue;
      }
      numQueens = numQueens + 1;
      if(points.length === numQueens) {
        break;
      }
    }
  }
  for(let i = 0; i < points.length; i++){
    board[points[i].r][points[i].c] = 1;
  }
  return board;
}

function checkRow(row){
  return row.some(x => x === 1);
}

function checkColumn(board, col) {
  return board.some(x => x[col] === 1);
}

function checkDiagonal(board, boardSize, row, col) {
  let c = col, r = row;
  while(col > 0) {
    r = r - 1;
    c = c - 1;
    col = col - 1;
  }
  for(; r < boardSize; r++) {
    for(; c < boardSize; c++) {
      if(board[r][c] === 1) {
        return true;
      }
    }
  }
  c = col;
  r = col;
  while(row < boardSize) {
    r = r + 1;
    c = c - 1;
    row = row - 1;
  }
  for(; r >= 0; r--) {
    for(; c < boardSize; c++) {
      if(board[r][c] === 1) {
        return true;
      }
    }
  }
  return false;
}