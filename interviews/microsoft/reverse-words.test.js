
const { reverseWords } = require('./reverse-words');

console.log(reverseWords(''));
console.log(reverseWords('abc'));
console.log(reverseWords('abc def'));
console.log(reverseWords('abc def ghi'));