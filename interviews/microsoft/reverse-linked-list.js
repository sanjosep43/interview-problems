
function reverseLinkedList(list) {
  let start = list;
  let prev = null;
  while(start) {
    let current = start;
    swapDirection(prev, current, current.next, current.next.next)
    list = list.next;
  }
  return ;
}

function swapDirection(prevA, a, b, nextB) {
  b.next = a;
  a.next = prevA;
  return nextB;
}

exports.reverseLinkedList = reverseLinkedList;