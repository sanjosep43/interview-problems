
const { secondLargestItem } = require('./binary-tree-second-largest-item');
const createBinaryTree = require('../../techie-delight/binary-tree/create-binary-tree');

let t1 = createBinaryTree([1,20,30,40,50,2,3,4,5]);
console.log(secondLargestItem(t1));

let t2 = createBinaryTree([44,20,30,40,50,2,3,4,5]);
console.log(secondLargestItem(t2));