import React from 'react';

const RecommendationAddCard = ({ text, onChange, addRecommendation }) => {
  return (
    <div className="recommendations__addcard">
      <span
        className="recommendations__add"
        onClick={addRecommendation}>
        &#x2795;
      </span>
      <textarea
        className="recommendations__text"
        value={text}
        placeholder="Add new recommendation"
        onChange={onChange}
      />
    </div>
  );
}

export default RecommendationAddCard;