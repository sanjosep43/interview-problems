import React from 'react';
import CancelOnUnmount from '../../../services/CancelOnUnmount.js'

const withAddRecommendation = ({ WrappedComponent, data }) => {
  return (props) => {
    const ref = useRef();
    const [newRecommendationText, setNewRecommendationText] = useState('');
    const addRecommendation = useCallback(() => {
      if (newRecommendationText.length === 0) {
        return;
      }
      const recommendationText = newRecommendationText;
      CancelOnUnmount.track(ref,
        service
          .addRecommendation(data.campaignId, recommendationText)
          .then(recommendation => {
            data.addRecommendation(recommendation)
          })
          .catch(() => {
            alert('Couldn\'t add recommendation, please try again.');
          })
      );
      setNewRecommendationText('');
    }, [newRecommendationText]);

    const handleTextAreaChanged = useCallback((e) => {
      setNewRecommendationText(e.target.value);
    }, [newRecommendationText]);

    return (
      <>
        <AddRecommendationCard
          addRecommendation={addRecommendation}
          text={newRecommendationText}
          onChange={handleTextAreaChanged}
        />
        <WrappedComponent {...props} />
      </>
    )
  };
};

export default withAddRecommendation;