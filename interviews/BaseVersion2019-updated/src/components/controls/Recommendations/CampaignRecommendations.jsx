import React, { useEffect, useState, useCallback, useRef } from 'react';
import CancelOnUnmount from '../../../services/CancelOnUnmount.js'
import '../../controls/Recommendations.scss';
import RecommendationCard from './RecommendationCard.jsx';
import AddRecommendationCard from './AddRecommendationCard.jsx';

// another way to design this component: Instead of passing in the showAddRecommendation prop, an HOC
//   could be created, withAddRecommendation, that would add the add recommendation card and handlers 
const CampaignRecommendations = React.memo(({ service, campaignId, mapData, showAddRecommendation }) => {
  const ref = useRef();
  const [recommendations, setRecommendations] = useState([]);
  const [newRecommendationText, setNewRecommendationText] = useState('');

  const addRecommendation = useCallback(() => {
    if (newRecommendationText.length === 0) {
      return;
    }
    const recommendationText = newRecommendationText;
    CancelOnUnmount.track(ref,
      service
        .addRecommendation(campaignId, recommendationText)
        .then(recommendation => {
          setRecommendations(mapData([recommendation]).concat(recommendations));
        })
        .catch(() => {
          alert('Couldn\'t add recommendation, please try again.');
        })
    );
    setNewRecommendationText('');
  }, [newRecommendationText]);

  const handleTextAreaChanged = useCallback((e) => {
    setNewRecommendationText(e.target.value);
  }, [newRecommendationText]);

  const dismissRecommendation = useCallback((id) => {
    CancelOnUnmount.track(ref,
      service.dismissRecommendation(campaignId, id)
        .then(recommendationId => {
          setRecommendations(recommendations.filter(x => x.id !== recommendationId));
        })
    );
  });

  useEffect(() => {
    CancelOnUnmount.track(ref,
      service
        .getAllRecommendations(campaignId)
        .then(recommendations => setRecommendations(mapData(recommendations)))
    );
    return () => CancelOnUnmount.handleUnmount(ref);
  }, []);

  return (
    <div className="recommendations">
      {showAddRecommendation &&
        <AddRecommendationCard
          addRecommendation={addRecommendation}
          text={newRecommendationText}
          onChange={handleTextAreaChanged}
        />
      }
      {recommendations.map(r =>
        <RecommendationCard
          key={r.id}
          id={r.id}
          text={r.text}
          dismiss={dismissRecommendation}
        />)
      }
    </div>
  );
});

export default CampaignRecommendations;
