import React from 'react';

const RecommendationCard = ({ id, text, dismiss }) => {
  return (
    <div className="recommendations__card">
      <span className="recommendations__dismiss" onClick={dismiss.bind(null, id)}>&#x2573;</span>
      <p>{text}</p>
    </div>
  );
}

export default RecommendationCard;