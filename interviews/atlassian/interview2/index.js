
function fetchAllFeatures() {
  // in reality, this would have been a `fetch` call:
  // `fetch("/api/features/all")`
  return new Promise((resolve) => {
    const features = {
      featureFoo: true,
      featureBar: false,
    };

    setTimeout(resolve, 100, features);
  });
}

// getFeatureState('extendedSummary').then(function (isEnabled) {
//   if (isEnabled) {
//     showExtendedSummary();
//   } else {
//     showBriefSummary();
//   }
// });

var cache = undefined;
var waiting = [];
var access = false;

async function getFeatureState(name) {
  if (!name) {
    console.log('empty name');
    return false;
  }
  if (!cache) {
    if (access) {
      const p = new Promise((resolve) => {
        return cache[name];
      });
      waiting.push(p);
      return p;
    } else {
      access = true;
    }
    try {
      cache = await fetchAllFeatures();
      for (let i = 0; i < waiting.length; i++) {
        waiting[i].resolve();
      }
    } catch (err) {
      console.log('error:', err);
      return false;
    }
  }
  if (cache[name] === undefined) {
    console.log('invalid name');
    return false;
  }
  return cache[name];
}

//getFeatureState('').then(result => console.log('empty:', result));
getFeatureState('abc').then(result => console.log('invalid:', result));
getFeatureState('featureFoo').then(result => console.log('featureFoo:', result));
//getFeatureState('featureBar').then(result => console.log('featureBar:', result));

// describe('feature state', () => {
//   it('empty string returns error', () => {

//   });
//   it('invalid feature name return error', () => {

//   });
//   it('valid feature name returns boolean value', () => {

//   });

// })

