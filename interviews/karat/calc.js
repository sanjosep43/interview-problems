const OPEN = 1;
const CLOSE = 2;
const NUM = 3;
const OPPLUS = 4;
const NOOP = 5;
const OPMINUS = 6;

function calc(input) {
  const stack = [];
  for (let i = 0; i < input.length; i++) {
    if (input[i] === '(') {
      stack.push({ value: '(', type: OPEN });
    } else if (input[i] === ')') {
      stack.push({ value: ')', type: CLOSE });
    } else if (input[i] === '+') {
      stack.push({ value: '+', type: OPPLUS });
    } else if (input[i] === '-') {
      stack.push({ value: '-', type: OPMINUS });
    } else {
      let num = '';
      while (Number.isInteger(parseInt(input[i]))) {
        num += input[i];
        ++i;
      }
      --i;
      stack.push({ value: parseInt(num), type: NUM });
    }
  }
  //console.log(stack);
  return calcHelper(stack);
}

function calcHelper(stack) {
  let total = 0;
  let currentOp = NOOP;
  while (stack.length > 0) {
    const token = stack.pop();
    if (token.type === NUM) {
      if (currentOp === NOOP) {
        total = token.value;
      } else if (currentOp === OPMINUS) {
        total = token.value - total;
      } else if (currentOp === OPPLUS) {
        total += token.value;
      }
    } else if (token.type === OPEN) {
      //?
    } else if (token.type === CLOSE) {
      const tempStack = [];
      let closeParenCount = 1;
      while (true) {
        if (stack.length === 0) {
          break;
        }
        const tempToken = stack.pop();
        if (tempToken.type === CLOSE) {
          closeParenCount += 1;
        } else if (tempToken.type === OPEN) {
          closeParenCount -= 1;
        }
        if (closeParenCount === 0) {
          break;
        }
        tempStack.push(tempToken);
      }
      total += calcHelper(tempStack);
    } else if (token.type === OPMINUS) {
      currentOp = OPMINUS;
    } else if (token.type === OPPLUS) {
      currentOp = OPPLUS;
    }
    //console.log('total:', total);
  }
  return total;
}

console.log(calc("(255)"));
console.log(calc("1+2-3"));
console.log(calc("255"));
console.log(calc("(1+2)-(1+2)"));
console.log(calc("1+((1-3)+(3+7))"));