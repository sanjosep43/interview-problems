import './style';
import { Component } from 'preact';
import Users from './components/users';
import AddNewUser from './components/add-new-user';
import Store from './store';
import MyTeam from './components/my-team';

export default class App extends Component {
	constructor() {
		super()
		this.store = new Store();
	}

	render() {
		return (
			<div class="container">
				<h1>User Accounts</h1>
				<h4>Add New User(s)</h4>
				<AddNewUser store={this.store} />
				<MyTeam store={this.store} />
				<Users store={this.store} />
			</div>
		);
	}
}
