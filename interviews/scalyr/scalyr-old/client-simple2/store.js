
import {extendObservable} from 'mobx'
import UserData from './user-data';

export default class Store {
  constructor() {
    extendObservable(this, {
      users: [],
    });
    this.userData = new UserData();
    this.accessLevel = '';
    this.email = '';
  }
  getUsers() {
    return this.userData.getUsers()
      .then(data => {
        this.users = data
        console.log('get users:', this.users.length);
        return this.users;
      });
  }
  addUser(user) {
    return this.userData.addUser(user)
      .then(data => {
        this.users.push(data);
        this.accessLevel = '';
        this.emails = '';
      })
  }
  revokeInviteOrAccess(user) {

  }
  resendInvite(user) {

  }
}
