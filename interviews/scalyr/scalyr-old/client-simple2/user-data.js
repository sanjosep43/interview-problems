
export default class UserData {
  getUsers() {
    return fetch('http://localhost:3000/users').then(response => response.json());
  }
  addUser(user) {
    return fetch('http://localhost:3000/users', {
      method: 'POST',
      body: JSON.stringify(user),
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json());
  }
  revokeInviteOrAccess(user) {

  }
  resendInvite(user) {

  }
}