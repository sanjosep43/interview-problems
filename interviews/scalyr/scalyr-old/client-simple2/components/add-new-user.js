import { Component } from 'preact';

export default class AddNewUser extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.props.store.accessLevel = '';
  }
  tokenize(str) {
    let items = str.split(' ');
    if(items.length === 1) {
      items = str.split(',');
    }
    return items;
  }
  handleSubmit(event) {
    event.preventDefault();
    if(!this.props.store.accessLevel) {
      alert('please choose access level');
      return;
    }
    this.props.store.addUser({
      email: this.props.store.emails,
      accessLevel: this.props.store.accessLevel,
      state: 'invited'
    });
  }

  handleChange(event) {
    this.props.store.accessLevel = event.target.value;
  }

  handleEmailChange(event) {
    this.props.store.emails = event.target.value;
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="email" placeholder="Enter one or more emails" onChange={this.handleEmailChange} />
        <select onChange={this.handleChange} name="accessLevel" placeholder="Select Access">
          <option value="" disabled selected>Select Access</option>
          <option value="full">Full</option>
          <option value="limited" >Limited</option>
          <option value="read-log" >Read Log</option>
        </select>
        <input type="submit" value="Send Invites" />
      </form>
    );
  }
}