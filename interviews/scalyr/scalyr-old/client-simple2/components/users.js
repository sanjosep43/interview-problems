import { Component } from 'preact';
import { observer } from 'mobx-observer';
import User from './user';

export default observer(class Users extends Component {
  componentDidMount() {
		this.props.store.getUsers();
  }

  render() {
    let elements = this.props.store.users.map(x => <User user={x} />);
    return (
      <div class="users-container">
        <div class="users-header">
          <span class="users-header-item">USER</span>
          <span class="users-header-item">ACCESS</span>
          <span class="users-header-item">ACTIONS</span></div>
        <div class="users">{elements}</div>
      </div>
    );
  }
})