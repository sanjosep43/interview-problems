import { Component } from 'preact';
import { observer } from 'mobx-observer';

export default observer(class Users extends Component {
  render() {
    return (
      <h4>My Team {this.props.store.users.length}</h4>
    );
  }
})