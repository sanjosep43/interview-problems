import { Component } from 'preact';
import mdl from 'material-design-lite/material';
import { Button } from 'preact-mdl';
import classNames from 'classnames';

export default class User extends Component {
  render() {
    let buttons = this.props.user.state === 'active' ?
      (<span><Button class="button">Revoke Access</Button></span>) :
      (<span><Button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Resend Invite</Button>
        <Button class="button">Revoke Invite</Button></span>);

    const stateClass = classNames({
      'oval': true,
      'user-row-item': true,
      'oval-active': this.props.user.state === 'active',
      'oval-inactive':this.props.user.state !== 'active'
    })
    const accessClass = 'user-row-item';
    return (
      <div class="user-row">
        <span class="user-row-item">
          <span class={stateClass}>{this.props.user.state} </span>
          <span>{this.props.user.email} </span>
        </span>
        <span class={accessClass}>{this.props.user.accessLevel} </span>
        {buttons}
      </div>
    )
  }
}