import './style';
import { Component } from 'preact';
import Users from './components/users';
import AddNewUser from './components/add-new-user';

export default class App extends Component {
	constructor(props) {
    super(props);
    this.state = {
			users: [],
			isLoading: false
		};
		this.addedNewUser = this.addedNewUser.bind(this);
		this.resendInvite = this.resendInvite.bind(this);
		this.revokeInvite = this.revokeInvite.bind(this);
	}

	addedNewUser(user) {
		this.setState({users: [...this.state.users, user]});
	}

	resendInvite(user) {
		console.log(user);
	}

	revokeInvite(user){
		console.log(user);
    let url = `http://localhost:3000/users/${user.id}`;
    return fetch(url, {
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
      },
      method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, cors, *same-origin
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // *client, no-referrer
    })
    .then(response => response.json()) // parses response to JSON
    .then(data => {
			console.log('deleted:', data)
			const index = this.state.users.findIndex(x => x.id === user.id);
			this.state.users.splice(index, 1);
			this.setState({users: this.state.users})
    });
	}

  componentDidMount() {
		this.setState({isLoading: true})
    fetch('http://localhost:3000/users')
      .then(response => response.json())
      .then(data => this.setState({ users: data, isLoading: false }));
	}

	render() {
		let {users, isLoading} = this.state;
		if(isLoading) {
			return <div>Loading...</div>;
		}
		return (
			<div class="main">
				<AddNewUser users={users} addedNewUser={this.addedNewUser} />
				<div>My Team</div>
				<Users users={users} resendInvite={this.resendInvite} revokeInvite={this.revokeInvite} />
			</div>
		);
	}
}
