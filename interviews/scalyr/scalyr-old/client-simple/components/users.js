import { Component } from 'preact';
import User from './user';

export default class Users extends Component {
	render() {
    let {users, revokeInvite, resendInvite} = this.props;
    let elements = users.map(x => <User user={x} resendInvite={resendInvite} revokeInvite={revokeInvite} />);
    return (
      <div class="container">
        <div class="header">
          <span class="header-item">USER</span>
          <span class="header-item">ACCESS</span>
          <span class="header-item">ACTIONS</span>
        </div>
        {elements}
      </div>
    );
	}
}
