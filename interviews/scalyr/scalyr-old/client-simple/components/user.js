import { Component } from 'preact';

export default class User extends Component {

	render() {
    let {user} = this.props;
		return (
			<div class="row">
        <div class="row-item">
          <span class="state">{user.state} </span>
          <span>{user.email}</span>
        </div>
        <div class="row-item">
          <span>{user.accessLevel}</span>
        </div>
        <div class="row-item">
          <button class="resend" onClick={() => this.props.resendInvite(user)}>Resend Invite </button>
          <button onClick={() => this.props.revokeInvite(user)}>Revoke Invite</button>
        </div>
			</div>
		);
	}
}
