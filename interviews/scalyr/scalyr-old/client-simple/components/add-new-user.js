import { Component } from 'preact';

export default class AddNewUser extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event){
    event.preventDefault();
    let data = {
      state: 'invited',
      email: this.state.email,
      accessLevel: this.state.accessLevel
    };
    let url = 'http://localhost:3000/users';
    return fetch(url, {
      body: JSON.stringify(data), // must match 'Content-Type' header
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, cors, *same-origin
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // *client, no-referrer
    })
    .then(response => response.json()) // parses response to JSON
    .then(data => {
      this.props.addedNewUser(data);
    });
  }
  handleChange(event){
    const target = event.target;
    const name = target.name;
    this.setState({
      [name]: target.value
    });
  }
	render() {
		return (
			<form onSubmit={this.handleSubmit} class="new-user">
        <input type="text"
              placeholder="Enter or more emails"
              name="email"
              value={this.state.email}
              onChange={this.handleChange} />
        <select name="accessLevel" onChange={this.handleChange} value={this.state.accessLevel}>
          <option value="full">Full</option>
          <option value="limited">Limited</option>
          <option value="read-only">Read-only</option>
        </select>
        <input type="submit" value="Send Invite" />
			</form>
		);
	}
}
