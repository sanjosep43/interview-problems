import { Component } from 'preact';
import UserService from '../user-service';

export default class AddNewUsers extends Component {
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.emailChange = this.emailChange.bind(this);
    this.accessChange = this.accessChange.bind(this);
    this.accessLevel = '';
    this.emails = '';
    this.userService = new UserService();
  }

  handleSubmit(event) {
    event.preventDefault();
    if(!this.emails || !this.accessLevel) {
      alert('inputs are wrong');
      return;
    }
    //validation of email address and creating the tokens should be separated out
    let emails = this.emails.split(' ');
    const regex = /\S+@\S+\.\S+/;  //got this from stackoverflow
    for(let i = 0; i < emails.length; i++) {
      if(!regex.test(emails[i])) {
        alert('email is wrong:'+emails[i]);
        return;
      }
    }
    this.userService.inviteUsers(emails, this.accessLevel)
      .then(result => {
        if(result.status === 'success') {
         this.props.addUsers(emails.map(x => {
           return {
             email: x,
             state: 'invited',
             accessLevel: this.accessLevel
           };
         }))
        } else {

        }
      })
  }

  emailChange(event) {
    this.emails = event.target.value;
  }

  accessChange(event){
    this.accessLevel = event.target.value;
  }

	render() {
		return (
        <div>
          <div class="user-title">Add New User(s)</div>
          <form onSubmit={this.handleSubmit}>
            <div class="add-new-user">
              <input class="new-user-email" type="text" onChange={this.emailChange} placeholder="Enter one or more emails" />
              <select class="new-user-select" onChange={this.accessChange}>
                <option value="" disabled selected>Select access</option>
                <option value="full">Full</option>
                <option value="read-only">Read Log</option>
                <option value="limited">Limited</option>
              </select>
              <input class="new-user-button send-invite" type="submit" value="Send Invites"/>
            </div>
          </form>
        </div>
		);
	}
}
