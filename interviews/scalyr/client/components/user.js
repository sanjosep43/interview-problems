import { Component } from 'preact';
import classNames from 'classnames';

export default class User extends Component {
	render() {
    const {user} = this.props;
    const buttons = user.state === 'invited' ?
      (<div><button class="button resend-invite" onClick={() => this.props.resendInvite(user)}>Resend Invite</button>
            <button class="button" onClick={() => this.props.revokeAccess(user)}>Revoke Invite</button></div>) :
      (<div><button class="button" onClick={() => this.props.revokeAccess(user)}>Revoke Access</button></div>);

    const stateClasses = classNames({
      'state': true,
      'state-invited': user.state === 'invited',
      'state-active': user.state !== 'invited',
    });
    const accessClasses = classNames ({
      'access': true,
      'access-full': user.accessLevel === 'full',
      'access-readonly': user.accessLevel === 'read-only',
      'access-limited': user.accessLevel === 'limited',
    });
  
		return (
      <div class="users">
        <span class="user-item">
        <span class={stateClasses}> {user.state}</span> {user.email}
        </span>
        <span class="user-item">
          <span class={accessClasses}>{user.accessLevel}</span>
        </span>
        <span class="user-item">
        {buttons}
        </span>
      </div>
		);
	}
}
