import { Component } from 'preact';

export default class MyTeam extends Component {
	render() {
    const {users} = this.props;
		return (
      <div class="my-team"><span class="team-title">My Team </span> <span class="my-team-count">{users.length}</span></div>
		);
	}
}
