import { Component } from 'preact';
import User from './user';

export default class Users extends Component {

	render() {
    const elements = this.props.users.map(x =>
      <User user={x} revokeAccess={this.props.revokeAccess}
                    resendInvite={this.props.resendInvite}
                    />);
		return (
			<div class="user-container">
        <div class="user-header">
          <span class="user-header-item">USER</span>
          <span class="user-header-item">ACCESS</span>
          <span class="user-header-item">ACTIONS</span>
        </div>
        <div>{elements}</div>
			</div>
		);
	}
}
