import './style';
import { Component } from 'preact';
import Users from './components/users';
import MyTeam from './components/myteam';
import UserService from './user-service';
import AddNewUsers from './components/add-new-users';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.userService = new UserService();
		this.state.users = [];
		this.revokeAccess = this.revokeAccess.bind(this);
		this.resendInvite = this.resendInvite.bind(this);
		this.addUsers = this.addUsers.bind(this);
	}

	sortUsers(a, b) {
		if(a.state === b.state) {
			if(a.email < b.email) {
				return -1;
			}
			return 1;
		}
		if(a.state === 'invited') {
			return -1;
		}
		return 1;
	}

  componentWillMount() {
    this.userService.getUsers()
      .then(data => {
				data.users.sort( (a,b) => {
					if(a.state === b.state) {
						if(a.email < b.email) {
							return -1;
						}
						return 1;
					}
					if(a.state === 'invited') {
						return -1;
					}
					return 1;
				});
        this.setState({users: data.users});
      })
	}

	addUsers(newUsers){
		this.setState({users: this.state.users.concat(newUsers)});
	}

	resendInvite(user){
		this.userService.resendInvite(user)
			.then(result => {
				console.log(result);
				if(result.status === "success") {
					console.log('invite resent');
				} else {

				}
			}
			);
	}

	revokeAccess(user) {
		console.log(user);
		this.userService.revokeAccess(user)
			.then(result => {
				console.log(result);
				if(result.status === "success") {
					let i = this.state.users.findIndex(x => x.email === user.email);
					this.state.users.splice(i, 1);
					this.setState({users: this.state.users});
				}
			})
	}

	render() {
		return (
			<div>
				<h1>User Accounts</h1>
				<AddNewUsers addUsers={this.addUsers} />
				<MyTeam users={this.state.users} />
				<Users users={this.state.users}
					revokeAccess={this.revokeAccess}
					resendInvite={this.resendInvite}
					/>
			</div>
		);
	}
}
