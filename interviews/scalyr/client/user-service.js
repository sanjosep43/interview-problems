
const Scalyr = require('./scalyrBackend');

export default class UserService {
  constructor() {
    this.scalyr = new Scalyr();
  }
  getUsers() {
    return this.scalyr.getUsers();
  }
  revokeAccess(user) {
    return this.scalyr.revokeAccess(user.email);
  }
  resendInvite(user){
    return this.scalyr.resendInvite(user.email);
  }
  revokeInvite(user){
    return this.scalyr.revokeAccess(user.email);
  }
  inviteUsers(users, accessLevel){
    return this.scalyr.inviteUsers(users, accessLevel);
  }
}