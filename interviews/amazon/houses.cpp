#include <iostream>
#include <vector>

using namespace std;

vector<int> cellCompete(int *states, int days)
{
  vector<int> result(8);
  bool first = true;
  for (int i = 0; i < days; i++)
  {
    //if neighbors on both sides are active or inactive
    //  then cell inactive
    //  else active
    // for ends, assume inactive
    if (first)
    {
      for (int s = 0; s < 8; s++)
      {
        if (s == 0)
        {
          if (states[1] == 0)
          {
            result[0] = 1;
          }
          else
          {
            result[0] = 0;
          }
        }
        else if (s == 7)
        {
          if (states[6] == 0)
          {
            result[7] = 1;
          }
          else
          {
            result[7] = 0;
          }
        }
        else
        {
          if ((states[s - 1] == 1 && states[s + 1] == 1) ||
              (states[s - 1] == 0 && states[s + 1] == 0))
          {
            result[s] = 0;
          }
          else
          {
            result[s] = 1;
          }
        }
      }
      first = false;
    }
    else
    {
    }
  }
  return result;
}