import {genNegative, genPositive, range} from '.';

describe('range', () => {
  describe('one argument', () => {
    it('return empty array with arg of 0', () => {
      const result = range(0);
      expect(result).toHaveLength(0);
    });
  });
  describe('with 2 arguments', () => {

  });
  describe('with 3 arguments', () => {
    it('returns the start element with step of 0', () => {
      const result = range(1, 5, 0);
      expect(result).toHaveLength(4);
      for(let i = 0; i < result.length; i++){
        expect(result[i]).toEqual(1)
      }
    })
  })
});

describe('gen', () => {
  it('increasing step', () => {
    const result = genPositive(0, 5, 1);
    expect(result).toHaveLength(5);
    expect(result[0]).toEqual(0);
    expect(result[4]).toEqual(4);
  });
  it('increasing steps at 3', () => {
    const result = genPositive(0, 10, 3);
    expect(result).toHaveLength(4);
    expect(result[0]).toEqual(0);
    expect(result[3]).toEqual(9);
  });
  it('increasing step from negative to positive', () => {
    const result = genPositive(-5, 5, 1);
    expect(result).toHaveLength(10);
    expect(result[0]).toEqual(-5);
    expect(result[9]).toEqual(4);
  });
  it('decreasing step', () => {
    const result = genNegative(5, 0, -1);
    expect(result).toHaveLength(5);
    expect(result[0]).toEqual(5);
    expect(result[4]).toEqual(1);
  });
  it('decreasing steps at 3', () => {
    const result = genNegative(10, 0, -3);
    expect(result).toHaveLength(4);
    expect(result[0]).toEqual(10);
    expect(result[3]).toEqual(1);
  });
  it('decreasing step from positive to negative', () => {
    const result = genNegative(5, -5, -1);
    expect(result).toHaveLength(10);
    expect(result[0]).toEqual(5);
    expect(result[9]).toEqual(-4);
  });
});
