import {describe, it} from 'node:test';
import {range} from './index';
import assert = require('node:assert');

describe('range', () => {
  it('with one argument of 0, returns an empty array', () => {
    const result = range(0);
    assert(result.length === 0);
  });

  it('with one positive argument, returns postive integers to that number', () => {
    const result = range(4);
    assert(result.length === 4);
    assert(result[0] === 0);
    assert(result[3] === 3);
  });

  it('with one negative argument, results 0 to that negative number', () => {
    const result = range(-4);
    assert(result.length === 4);
    assert(result[0] === 0);
    assert(result[3] === -3);
  });

  it('returns postive number from start to end', () => {
    const result = range(1, 5);
    assert(result.length === 4);
    assert(result[0] === 1);
    assert(result[3] === 4);
  });

  it('returns negative numbers when start is negative', () => {
    const result = range(0, -4);
    assert(result.length === 4);
    assert(result[0] === 0);
    assert(result[3] === -3);
  });

  it('with 3 args, returns positive numbers with step', () => {
    const result = range(1, 8, 2);
    assert(result.length === 4);
    assert(result[0] === 1);
    assert(result[3] === 7);
  });

  it('with 3 args, returns negative numbers with step', () => {
    const result = range(0, -8, 2);
    assert(result.length === 4);
    assert(result[0] === 0);
    assert(result[3] === -6);
  });

  it('with 3 args, step is 0 then return same start index', () => {
    const result = range(0, -8, 0);
    assert(result.length === 8);
    assert(result[0] === 0);
    assert(result[7] === 0);
  });

  it('with 3 args, step is 0 then return same start index', () => {
    const result = range(1, 4, 0);
    assert(result.length === 3);
    assert(result[0] === 1);
    assert(result[2] === 1);
  });
});
