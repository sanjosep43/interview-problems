/* eslint-disable @typescript-eslint/no-unused-vars */
// questions, code, solution writeups, and test cases in TypeScript.

export function range2(...args: number[]): Array<number> {
  if (args.length === 1) {
    let positive = false;
    if (args[0] >= 0) positive = true;
    let num = 0;
    return Array(positive ? args[0] : -1 * args[0])
      .fill(0)
      .map(_ => (positive ? num++ : num--));
  } else if (args.length === 2) {
    const start = args[0];
    const end = args[1];
    if (end >= start) {
      let num = start;
      return Array(end - start)
        .fill(0)
        .map(_ => num++);
    } else {
      let num = start;
      return Array(Math.abs(end) - Math.abs(start))
        .fill(0)
        .map(_ => num--);
    }
  } else if (args.length === 3) {
    let start = args[0];
    const end = args[1];
    const step = args[2];
    if (step === 0) {
      return Array(Math.abs(Math.abs(end) - Math.abs(start)))
        .fill(0)
        .map(_ => start);
    }
    if (end >= start) {
      return Array(Math.round((end - start) / step))
        .fill(0)
        .map(_ => {
          const r = start;
          start += step;
          return r;
        });
    } else {
      return Array(Math.round((Math.abs(end) - Math.abs(start)) / step))
        .fill(0)
        .map(_ => {
          const r = start;
          start -= step;
          return r;
        });
    }
  }
  return [];
}

export function range(...args: number[]): Array<number> {
  if (args.length === 1) {
    return genPositive(0, args[0], 1);
  } else {
    const start = args[0];
    const end = args[1];
    let step = args.length === 2 ? 1 : args[2];
    if (step === 0) {
      return Array(getRange(start, end)).fill(start);
    }
    if (start <= end) {
      return genPositive(start, end, step);
    } else {
      if (step === 1) step *= -1;
      return genNegative(start, end, step);
    }
  }
}

function getRange(start: number, end: number) {
  return Math.abs(Math.abs(end) - Math.abs(start));
}

export function genPositive(start: number, end: number, step: number) {
  const output = [];
  for (let i = start; i < end; i += step) {
    output.push(i);
  }
  return output;
}
export function genNegative(start: number, end: number, step: number) {
  const output = [];
  for (let i = start; i > end; i += step) {
    output.push(i);
  }
  return output;
}
