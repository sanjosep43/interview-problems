"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_test_1 = require("node:test");
const index_1 = require("./index");
const assert = require("node:assert");
(0, node_test_1.describe)('range', () => {
    (0, node_test_1.it)('with one argument of 0, returns an empty array', () => {
        const result = (0, index_1.range)(0);
        assert(result.length === 0);
    });
    (0, node_test_1.it)('with one positive argument, returns postive integers to that number', () => {
        const result = (0, index_1.range)(4);
        assert(result.length === 4);
        assert(result[0] === 0);
        assert(result[3] === 3);
    });
    (0, node_test_1.it)('with one negative argument, results 0 to that negative number', () => {
        const result = (0, index_1.range)(-4);
        assert(result.length === 4);
        assert(result[0] === 0);
        assert(result[3] === -3);
    });
    (0, node_test_1.it)('returns postive number from start to end', () => {
        const result = (0, index_1.range)(1, 5);
        assert(result.length === 4);
        assert(result[0] === 1);
        assert(result[3] === 4);
    });
    (0, node_test_1.it)('returns negative numbers when start is negative', () => {
        const result = (0, index_1.range)(0, -4);
        assert(result.length === 4);
        assert(result[0] === 0);
        assert(result[3] === -3);
    });
    (0, node_test_1.it)('with 3 args, returns positive numbers with step', () => {
        const result = (0, index_1.range)(1, 8, 2);
        assert(result.length === 4);
        assert(result[0] === 1);
        assert(result[3] === 7);
    });
    (0, node_test_1.it)('with 3 args, returns negative numbers with step', () => {
        const result = (0, index_1.range)(0, -8, 2);
        assert(result.length === 4);
        assert(result[0] === 0);
        assert(result[3] === -6);
    });
    (0, node_test_1.it)('with 3 args, step is 0 then return same start index', () => {
        const result = (0, index_1.range)(0, -8, 0);
        assert(result.length === 8);
        assert(result[0] === 0);
        assert(result[7] === 0);
    });
    (0, node_test_1.it)('with 3 args, step is 0 then return same start index', () => {
        const result = (0, index_1.range)(1, 4, 0);
        assert(result.length === 3);
        assert(result[0] === 1);
        assert(result[2] === 1);
    });
});
//# sourceMappingURL=index.test.js.map