"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
describe('range', () => {
    describe('one argument', () => {
        it('return empty array with arg of 0', () => {
            const result = (0, _1.range)(0);
            expect(result).toHaveLength(0);
        });
    });
    describe('with 2 arguments', () => {
    });
    describe('with 3 arguments', () => {
    });
});
describe('gen', () => {
    it('increasing step', () => {
        const result = (0, _1.genPositive)(0, 5, 1);
        expect(result).toHaveLength(5);
        expect(result[0]).toEqual(0);
        expect(result[4]).toEqual(4);
    });
    it('increasing steps at 3', () => {
        const result = (0, _1.genPositive)(0, 10, 3);
        expect(result).toHaveLength(4);
        expect(result[0]).toEqual(0);
        expect(result[3]).toEqual(9);
    });
    it('increasing step from negative to positive', () => {
        const result = (0, _1.genPositive)(-5, 5, 1);
        expect(result).toHaveLength(10);
        expect(result[0]).toEqual(-5);
        expect(result[9]).toEqual(4);
    });
    it('decreasing step', () => {
        const result = (0, _1.genNegative)(5, 0, -1);
        expect(result).toHaveLength(5);
        expect(result[0]).toEqual(5);
        expect(result[4]).toEqual(1);
    });
    it('decreasing steps at 3', () => {
        const result = (0, _1.genNegative)(10, 0, -3);
        expect(result).toHaveLength(4);
        expect(result[0]).toEqual(10);
        expect(result[3]).toEqual(1);
    });
    it('decreasing step from positive to negative', () => {
        const result = (0, _1.genNegative)(5, -5, -1);
        expect(result).toHaveLength(10);
        expect(result[0]).toEqual(5);
        expect(result[9]).toEqual(-4);
    });
});
//# sourceMappingURL=range.test.js.map