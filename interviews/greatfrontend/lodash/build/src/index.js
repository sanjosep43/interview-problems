"use strict";
/* eslint-disable @typescript-eslint/no-unused-vars */
// questions, code, solution writeups, and test cases in TypeScript.
Object.defineProperty(exports, "__esModule", { value: true });
exports.genNegative = exports.genPositive = exports.range = exports.range2 = void 0;
// Creates an array of numbers (positive and/or negative) progressing
// from start up to, but not including, end.
// A step of -1 is used if a negative start is specified without an end
// or step. If end is not specified, it's set to start with start then
// set to 0.
function range2(...args) {
    if (args.length === 1) {
        let positive = false;
        if (args[0] >= 0)
            positive = true;
        let num = 0;
        return Array(positive ? args[0] : -1 * args[0])
            .fill(0)
            .map(_ => (positive ? num++ : num--));
    }
    else if (args.length === 2) {
        const start = args[0];
        const end = args[1];
        if (end >= start) {
            let num = start;
            return Array(end - start)
                .fill(0)
                .map(_ => num++);
        }
        else {
            let num = start;
            return Array(Math.abs(end) - Math.abs(start))
                .fill(0)
                .map(_ => num--);
        }
    }
    else if (args.length === 3) {
        let start = args[0];
        const end = args[1];
        const step = args[2];
        if (step === 0) {
            return Array(Math.abs(Math.abs(end) - Math.abs(start)))
                .fill(0)
                .map(_ => start);
        }
        if (end >= start) {
            return Array(Math.round((end - start) / step))
                .fill(0)
                .map(_ => {
                const r = start;
                start += step;
                return r;
            });
        }
        else {
            return Array(Math.round((Math.abs(end) - Math.abs(start)) / step))
                .fill(0)
                .map(_ => {
                const r = start;
                start -= step;
                return r;
            });
        }
    }
    return [];
}
exports.range2 = range2;
function range(...args) {
    if (args.length === 1) {
        return genPositive(0, args[0], 1);
    }
    else {
        const start = args[0];
        const end = args[1];
        let step = args.length === 2 ? 1 : args[2];
        if (step === 0) {
            return Array(getRange(start, end)).fill(start);
        }
        if (start <= end) {
            return genPositive(start, end, step);
        }
        else {
            if (step === 1)
                step *= -1;
            return genNegative(start, end, step);
        }
    }
}
exports.range = range;
function getRange(start, end) {
    return Math.abs(Math.abs(end) - Math.abs(start));
}
function genPositive(start, end, step) {
    const output = [];
    for (let i = start; i < end; i += step) {
        output.push(i);
    }
    return output;
}
exports.genPositive = genPositive;
function genNegative(start, end, step) {
    const output = [];
    for (let i = start; i > end; i += step) {
        output.push(i);
    }
    return output;
}
exports.genNegative = genNegative;
/*
_.range(4);
// => [0, 1, 2, 3]

_.range(-4);
// => [0, -1, -2, -3]

_.range(1, 5);
// => [1, 2, 3, 4]

_.range(0, 20, 5);
// => [0, 5, 10, 15]

_.range(0, -4, -1);
// => [0, -1, -2, -3]

_.range(1, 4, 0);
// => [1, 1, 1]

_.range(0);
// => []
*/
//# sourceMappingURL=index.js.map