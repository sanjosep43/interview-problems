export declare function range2(...args: number[]): Array<number>;
export declare function range(...args: number[]): Array<number>;
export declare function genPositive(start: number, end: number, step: number): number[];
export declare function genNegative(start: number, end: number, step: number): number[];
