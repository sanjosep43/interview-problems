// Find Kth minimum node in a binary tree and suggest a complexity

function kthMin(root, kth) {
  let result = [];
  kthMinHelper(root, kth, result);
  return result[kth-1]; 
}

function kthMinHelper(node, n, results) {
  if(!node) {
    return false;
  }
  results.push(node.data);
  if(results.length === n) {
    return true;
  }
  if(!kthMinHelper(node.left, n, results)){
    return kthMinHelper(node.right, n, results);
  }
  return true;
}
