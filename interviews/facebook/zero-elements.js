/*
Given an array of integers:
1. rearrange the array such that all non-zero members appear on the left of the array (order is not important)
2. return the number of non-zero members

e.g. [1,2,0,5,3,0,4,0] => [1,2,5,3,4,0,0,0] and return 5. The non-zero array members can be in any order.
*/

function moveZeroElements(a) {
  let count = 0;
  let zeroIndex = a.length - 1;
  while(a[zeroIndex] === 0 && zeroIndex > 0) {
    zeroIndex -= 1;
    count += 1;
  }
  for(let i = 0; i < a.length && i < zeroIndex+1; i++){
    if(a[i] === 0){
      let temp = a[i];
      a[i] = a[zeroIndex];
      a[zeroIndex] = temp;
      zeroIndex -= 1;
      count += 1;
    } else {
    }
  }
  return a.length - count;
}

let a, result;
a = [1];
result = moveZeroElements(a);
console.log(result, a);

a = [0];
result = moveZeroElements(a);
console.log(result, a);

a = [1, 0];
result = moveZeroElements(a);
console.log(result, a);

a = [0, 1];
result = moveZeroElements(a);
console.log(result, a);

a = [1,2,0,5,3,0,4,0];
result= moveZeroElements(a);
console.log(result, a);

a = [1,2,0,5,3,0,0,0];
result= moveZeroElements(a);
console.log(result, a);