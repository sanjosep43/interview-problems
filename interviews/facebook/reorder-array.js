
function reOrderArray(a, b) {
  let swapped = [];
  for(let i = 0; i < a.length; i++){
    swapped.push(false);
  }
  for(let i = 0; i < b.length; i++){
    let realIdxOfA = b[i];
    if(!swapped[realIdxOfA]){
      let temp = a[realIdxOfA];
      a[realIdxOfA] = a[i];
      a[i] = temp;
      swapped[realIdxOfA] = true;
      if(b[realIdxOfA] === i){
        swapped[i] = true;
      }
      console.log(a);
      console.log(swapped);
    }
  }
}
let A = ['A', 'B'];
let B = [1, 0];
// reOrderArray(A,B);
// console.log(A);

A = ['C', 'D', 'E', 'F', 'G'];
B = [ 3,   0,   4,   1,   2];
reOrderArray(A, B);
console.log(A);
// A is now [D, F, G, C, E];
