
function printTree(node) {
  let lists = {};
  printTreeHelper(node, 0, lists);
  for(let k in lists) {
    console.log(lists[k]);
  }
}

function printTreeHelper(node, level, lists) {
  if(!node) {
    return;
  }
  printTreeHelper(node.right, level+1, lists);
  printTreeHelper(node.left, level+1, lists);
  if(!lists[level]){
    lists[level] = node.data;
  } else {
    lists[level] += node.data;
  }
}

let node = {
  data: 'A',
  left: {
    data: 'B',
    left: {
      data: 'D'
    },
    right: {
      data: 'E'
    }
  },
  right: {
    data: 'C',
    left: {
      data: 'F'
    },
    right: {
      data: 'G'
    }
  }
};
printTree(node);