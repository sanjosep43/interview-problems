
function totalDistinctNodes(g, s, d) {

}

let g = [
  {
    name: 'a',
    adjacencyList: ['b', 'c', 'f']
  },
  {
    name: 'b',
    adjacencyList: ['a', 'e']
  },
  {
    name: 'c',
    adjacencyList: ['a', 'f']
  },
  {
    name: 'e',
    adjacencyList: ['b']
  },
  {
    name: 'f',
    adjacencyList: ['a', 'c']
  }
];

console.log(totalDistinctNodes(g, 'a', 'f'));