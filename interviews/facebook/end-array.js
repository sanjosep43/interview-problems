/*
You are given an array of non-negative integers (0, 1, 2 etc).
 The value in each element represents the number of hops you may take to the next destination.
  Write a function that determines when you start from the first element whether you will be able
   to reach the last element of the array.
if a value is 3, you can take either 0, 1, 2 or 3 hops.
For eg: for the array with elements 1, 2, 0, 1, 0, 1, any route you take from the first element,
 you will not be able to reach the last element.
*/

function endArray(a, start=0) {
  for(let i = start; i < a.length; i++) {
    if(a[i] === 0) {
      return false;
    }
    if(a[i] === 1) {
      continue;
    }
    for(let x = 1; x <= a[i]; x++){
      endArray(a, start+x);
    }
  }
  return true;
}

console.log(endArray([1, 0]));
console.log(endArray([1,2,0,1,0,1]));