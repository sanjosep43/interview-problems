//Write code to multiply two large numbers given as strings (without using BigInteger).

function addStrings(s1, s2) {
  let result = [];
  let carry = 0;
  for(let i = s1.length-1; i >= 0; i--) {
    let n1 = parseInt(s1[i]);
    let n2 = parseInt(s2[i]);
    let r = n1 + n2 + carry;
    if(n1 + n2 > 9) {
      carry = 1;
      r = r % 10;
    } else {
      carry = 0;
    }
    result.push(r);
  }
  return result.reverse().join('');
}

function multiplyStrings(s1, s2) {
  
}

console.log(addStrings('123', '123'));