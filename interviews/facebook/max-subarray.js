
function isSumExist(a, sizeM, total) {
  for(let i = 0; i < a.length - sizeM; i++) {
    let sum = 0;
    for(let e = i; e < i + sizeM; e++) {
      sum += a[e];
    }
    if(sum === total) {
      return true;
    }
  }
  return false;
}