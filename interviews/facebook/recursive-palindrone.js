
function palindrone(str) {
  let left = 0;
  let right = str.length - 1;
  return palindroneHelper(str, left, right);
}

function palindroneHelper(str, left, right) {
  if(left === right || left > right){
    return true;
  }
  return str[left] === str[right] && palindroneHelper(str, left+1, right-1);
}

console.log(palindrone('abc'));
console.log(palindrone('aba'));
console.log(palindrone('abba'));
console.log(palindrone('aaabbb'));
console.log(palindrone('aacbb'));
console.log(palindrone('abcba'));