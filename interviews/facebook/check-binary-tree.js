
function isBinarySearchTree(node) {
  if(!node){
    return true;
  }
  if( node.left && (node.left.data >= node.data) ||
      node.right && (node.right.data < node.data)) {
    return false;
  }
  return isBinarySearchTree(node.left) ? isBinarySearchTree(node.right) : false;
}

let node = {
  data: 100,
  left: {
    data: 70,
    left: {
      data: 65 
    },
    right: {
      data: 72
    }
  },
  right: {
    data: 120,
    left: {
      data: 119 
    },
    right: {
      data: 122
    }
  }
};

console.log(isBinarySearchTree(node));

node = {
  data: 100,
  left: {
    data: 70,
    left: {
      data: 71
    },
    right: {
      data: 72
    }
  },
  right: {
    data: 120,
    left: {
      data: 119 
    },
    right: {
      data: 122
    }
  }
};

console.log(isBinarySearchTree(node));