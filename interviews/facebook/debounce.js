
function debounce(event, func, timeout) {
  document.addEventListener(eventStr, func);
  setInterval(
    function(){
      func();
      document.removeEventListener(eventStr, func);
      setTimeout(function(){
        document.addEventListener(eventStr, func);
      }, timeout);
  }, timeout);
}