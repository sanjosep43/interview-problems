
function merge(a, b) {
  let result = [];
  let shorterLen = a.length > b.length ? b.length : a.length;
  let aIdx = 0;
  let bIdx = 0;
  for(aIdx = 0, bIdx = 0; aIdx < shorterLen && bIdx < shorterLen; ) {
    if (a[aIdx] <= b[bIdx]) {
      result.push(a[aIdx]);
      aIdx++;
    } else {
      result.push(b[bIdx]);
      bIdx++;
    }
  }
  console.log(aIdx, a.length, bIdx, b.length);
  if(aIdx === a.length-1) {
    while(bIdx < b.length) {
      if(b[bIdx] <= a[aIdx]) {
        result.push(b[bIdx]);
        bIdx++;
      } else {
        result.push(a[aIdx]);
      }
    }
  } else {
    while(aIdx < a.length) {
      if(a[aIdx] <= b[bIdx]) {
        result.push(a[aIdx]);
        aIdx++;
      } else {
        result.push(b[bIdx]);
      }
    }
  }
  return result;
}

console.log(merge([9], [4,6]));
// console.log(merge([1,2,3], [4,5,6]));
// console.log(merge([9,10], [4,6,8]));
// console.log(merge([4,6,8], [9,10]));
// console.log(merge([1,3,5], [2,4,6]));