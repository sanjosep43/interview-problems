
function flatten(a) {
  let result = [];
  for(let i = 0; i < a.length; i++){
    result = result.concat(a[i]);
  }
  return result;
}

console.log(flatten([1,2,3]))
console.log(flatten([1,[2],[3,4]]))