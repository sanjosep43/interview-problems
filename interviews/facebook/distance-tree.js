
// function distanceBetweenNodes(node, a, b, foundA=false, distance=0) {
//   if(!node) {
//     return 0;
//   }
//   if(node.data === a) {
//     if(foundA) {}
//     foundA = true;
//   }
//   if(foundA && node.data === b) {
//     return distance;
//   }
//   if(!distanceBetweenNodes(node.left, a, b, foundA, foundA ? distance : distance+1)){
//     return distanceBetweenNodes(node.right, a, b, foundA, foundA ? distance : distance+1);
//   }
// }

function distanceBetweenNodes(node, a, b) {
  let dequeue = [];
  dequeue.push(node);
  let foundA = false;
  let distance = 0;
  while(dequeue.length !== 0) {
    let e = dequeue.shift();
    if(foundA){
      distance += 1;
      if(e.data === b){
        return distance;
      }
    }
    if(e.data === a) {
      foundA = true;
    }
    if(e.left) dequeue.push(e.left);
    if(e.right) dequeue.push(e.right);
  }
}

let node = {
  data: 'A',
  left: {
    data: 'B',
    left: {
      data: 'D'
    },
    right: {
      data: 'E'
    }
  },
  right: {
    data: 'C',
    left: {
      data: 'F'
    },
    right: {
      data: 'G'
    }
  }
};

console.log(distanceBetweenNodes(node, 'A', 'G'));
console.log(distanceBetweenNodes(node, 'E', 'G'));