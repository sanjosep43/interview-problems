function generate(colors, size) {
  const results = [];
  let colorIdx = 0;
  while (results.length < size) {
    results.push(colors[colorIdx]);
    colorIdx = getNextColorIdx(colorIdx, colors);
    if (isLastAdjacentMatching(results)) {
      results.pop();
      results.push(colors[colorIdx]);
    }
    while (isThreeRepeats(results)) {
      results.pop();
      results.push(colors[colorIdx]);
      colorIdx = getNextColorIdx(colorIdx, colors);
    }
  }
  return results;
}

function getNextColorIdx(colorIdx, colors) {
  colorIdx += 1;
  if (colorIdx >= colors.length) {
    colorIdx = 0;
  }
  return colorIdx;
}

function isLastAdjacentMatching(input) {
  if (input.length <= 1) {
    return false;
  }
  return input[input.length - 1] === input[input.length - 2];
}

function isThreeRepeats(input) {
  if (input.length <= 3) {
    return false;
  }
  for (let x = 0; x < input.length - 2; x++) {
    for (let y = x + 1; y < input.length - 2; y++) {
      if (input[x] === input[y] &&
        input[x + 1] === input[y + 1] &&
        input[x + 2] === input[y + 2]) {
        return true;
      }
    }
  }
  return false;
}

const input1 = ["R", "Y"];
let result = generate(input1, 3); // R x R
console.log(result);

const input2 = ["R", "Y", "G"];
result = generate(input2, 4); // R x R x
console.log(result);

const input3 = ["R", "Y", "G", "B"];  // 8
result = generate(input3, 8); // R x R x R x R x
console.log(result);

const input4 = ["R", "Y", "G", "B"];  // 8
result = generate(input4, 16); // R x R x R x R x  Y x Y x Y x
console.log(result);