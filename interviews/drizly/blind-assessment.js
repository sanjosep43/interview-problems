// language: Javascript

// using node's assert library for testing
const assert = require('assert').strict;

const isEven = n => n % 2 === 0;

function findMedian(input) {
  //in place sorting of input
  input.sort((a, b) => {
    if (a < b) { return -1; }
    else if (a > b) { return 1; }
    return 0;
  });

  // subract 1 from length of input to get the index which is zero-based
  const indexLen = input.length - 1;
  if (isEven(input.length)) {
    // example: if length is 4, then indexLen is 3, and we want to get input[1] & input[2]
    // . dividing an odd number (3 in this case) by 2 will get 1.5 so we need to get the floor & ceil
    const leftIdx = Math.floor(indexLen / 2);
    const rightIdx = Math.ceil(indexLen / 2);
    return (input[leftIdx] + input[rightIdx]) / 2;
  } else {
    return input[Math.floor(indexLen / 2)];
  }
}

assert.strictEqual(findMedian([5]), 5);
assert.strictEqual(findMedian([6, 5]), 5.5);
assert.strictEqual(findMedian([1, 6, 3, 5, 8, 9, 4, 10, 2]), 5);
assert.strictEqual(findMedian([1, 6, 3, 5, 8, 9, 4, 10, 2, 7]), 5.5);

/*
As it pertains to the role you are interviewing for, please describe a time when a
technical project or assignment didn’t go as planned. How would you approach the
situation differently in the future? Please include information about the purpose of
the project, the people involved, what went wrong, the consequences of it not
going as planned and how you would do things differently in the future.

I was tasked with finding performance problems as early as possible before the Groupon website (homepage) could
be pushed to production.  The project was tasked to just me as a research project.  The result of the project was
a failure to find a reliable way to measure the performance of the current changes.
I tried to take a pull request and after the code built in CI, then I used the build artifact (a docker container)
and started the site and ran measurements.  I then ran the current version (latest master) and then compared values.
I could not get values that were close enough to show some consistently between the builds.
What I could have done differently is the following:  I should have spent more time building a similar system
that could have worked locally on my computer.  That would have resulted in faster iterations.  Having to wait
for CI to pick up the build and the length of time it took to finish was frustrating.
The consquences of not being able to get this working correctly was in the end positive.  I was able to eventually
iterate on a solution that involved using another server which did not use the current PR but would make
measurements against the staging environments.

*/