const assert = require('assert').strictEqual;

function htmlValidator(html) {
  const stack = [];
  const openTagRe = /<(\w+)>/;
  const closeTagRe = /<\/(\w+)>/;
  const selfEnclosedTagRe = /(<\w+.*\/>)/;
  for (let i = 0; i < html.length; i++) {
    if (html[i] === '<') {
      if(i+1 === html.length) {
        return true;
      }
      const s1 = html.substring(i);
      const matchOpen = s1.match(openTagRe);
      if (matchOpen) {
        stack.push(matchOpen[1]);
        i += matchOpen[0].length - 1;
        continue;
      }
      const matchSelf = s1.match(selfEnclosedTagRe);
      if (matchSelf) {
        i += matchSelf[0].length - 1;
        continue;
      }
      const matchClose = s1.match(closeTagRe);
      if (matchClose) {
        if (stack.length === 0) {
          return false;
        }
        const openTag = stack.pop();
        if(openTag !== matchClose[1]) {
          return false;
        }
        i += matchClose[0].length - 1;
        continue;
      }
    }
  }
  return stack.length === 0;
}

assert(htmlValidator(''), true);
assert(htmlValidator('abc def '), true);
assert(htmlValidator('<tag></tag>'), true);
assert(htmlValidator('<tag>I love coding <Component />!</tag>'), true);
assert(htmlValidator('<tag><tag>'), false);
assert(htmlValidator('<tag>'), false);
assert(htmlValidator('</tag>'), false);