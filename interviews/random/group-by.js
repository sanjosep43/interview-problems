const assert = require('assert').strict;

function groupBy(array, fn) {
  const obj = {};
  array.forEach(x => {
    const key = fn(x);
    if(!obj[key]) {
      obj[key] = [x];
    } else {
      obj[key].push(x);
    }
  });
  return obj;
}

assert.deepStrictEqual(
  groupBy([6.1, 4.2, 6.3], Math.floor), 
  { '4': [4.2], '6': [6.1, 6.3] }
);