import React from "react";
import './TicTacToe.css';

type RowProps = {
  rowNum: number;
  onClick: (x: number, y: number) => void;
  row: Array<string>;
}

const Row = ({rowNum, onClick, row}: RowProps) => {
  return (
    <div>
      <span className='cell' onClick={() => onClick(rowNum, 0)}>{row[0]}</span>
      <span className='cell' onClick={() => onClick(rowNum, 1)}>{row[1]}</span>
      <span className='cell' onClick={() => onClick(rowNum, 2)}>{row[2]}</span>
    </div>
  );
}

const TicTacToe = () => {
  const [playerTurn, setPlayerTurn] = React.useState('X');
  const [board, setBoard] = React.useState([[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]);
  // const [winner, setWinner] = React.useState('');
  const onClick = React.useCallback((x:number, y:number) => {
    if(board[x][y] !== ' ') return;

    setBoard(prevBoard => {
      const newBoard = [...prevBoard];
      newBoard[x][y] = playerTurn;
      // setWinner(checkWinner(newBoard));
      setPlayerTurn(playerTurn === 'X' ? 'O' : 'X');
      return newBoard;
    })}, [setPlayerTurn, playerTurn, setBoard, board]);

  return (
    <div>
      {board.map((row, idx) => <Row rowNum={idx} onClick={onClick} row={row} />)}
    </div>
  );
}


export default TicTacToe;