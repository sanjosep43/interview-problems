let promise = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error('ERROR')), 1000);
});

promise
  .then(
    (result) => console.log(result),
    (error) => console.log(error)
  )
  .catch((e) => {
    console.log('caught', e);
  })
  .finally(() => console.log('finally'));

const sleep = (callback, time) => {
  setTimeout(() => {
    callback();
  }, time);
};

function promisifySleep(callback, arg) {
  return new Promise((resolve, reject) => {
    resolve(sleep(callback, arg));
  });
}

console.log('psleep starting');
const psleep = promisifySleep(() => console.log('psleep done'), 1000);
psleep.then((r) => console.log('psleep resolved'));
