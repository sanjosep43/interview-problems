function* gen() {
  for (let i = 0; i < 10; i++) {
    yield i;
  }
}

// const g = gen();
// console.log(g.next());

for (const v of gen()) {
  console.log(v);
}
