import Paper from '@material-ui/core/Paper';
import React from 'react';
import './CreditCard.css';

const CreditCard = () => {
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  // const [credit, setCredit] = React.useState('');
  const [displayCredit, setDisplayCredit] = React.useState('');
  const [month, setMonth] = React.useState('');
  const [year, setYear] = React.useState('');
  const [securityCode, setSecurityCode] = React.useState('');
  const onCreditChange = React.useCallback(
    (ev) => {
      if (ev.target.value.length === 20) {
        return;
      }
      const lastSpaceIdx = displayCredit.lastIndexOf(' ');
      if (lastSpaceIdx !== -1) {
        if (displayCredit.length - 1 === ev.target.value.length) {
          setDisplayCredit(ev.target.value);
          return;
        }
      }
      if (ev.target.value.length === 4) {
        setDisplayCredit(ev.target.value + ' ');
      } else if (ev.target.value.length === 9) {
        setDisplayCredit(ev.target.value + ' ');
      } else if (ev.target.value.length === 14) {
        setDisplayCredit(ev.target.value + ' ');
      } else {
        setDisplayCredit(ev.target.value);
      }
    },
    [displayCredit]
  );
  const onKeyDown = React.useCallback((ev) => {
    if (ev.key === 'Backspace') {
      return;
    }
    if (isNaN(parseInt(ev.key))) {
      ev.preventDefault();
      ev.stopPropagation();
      return;
    }
    console.log('down:', ev.key);
  }, []);
  const onMonthChange = React.useCallback((ev) => {
    if (ev.target.value.length > 2) {
      return;
    }
    setMonth(ev.target.value);
  }, []);
  const onYearChange = React.useCallback((ev) => {
    if (ev.target.value.length > 4) {
      return;
    }
    setYear(ev.target.value);
  }, []);
  const onSecurityCodeChange = React.useCallback((ev) => {
    if (ev.target.value.length > 4) {
      return;
    }
    setSecurityCode(ev.target.value);
  }, []);
  const onFirstNameChange = React.useCallback((ev) => {
    setFirstName(ev.target.value);
  }, []);
  const onLastNameChange = React.useCallback((ev) => {
    setLastName(ev.target.value);
  }, []);
  return (
    <Paper variant="outlined" square>
      <form>
        <div className="container2">
          <div className="nameContainer">
            <input
              className="firstName"
              placeholder="first name"
              value={firstName}
              onChange={onFirstNameChange}
            />
            <input placeholder="last name" value={lastName} onChange={onLastNameChange} />
          </div>
          <div className="cardContainer">
            <div className="numberContainer">
              <input
                placeholder="0123 4567 8901 2323"
                value={displayCredit}
                onKeyDown={onKeyDown}
                onChange={onCreditChange}
              />
              <div className="numberContainer2">
                <input
                  className="month"
                  placeholder="00"
                  onKeyDown={onKeyDown}
                  value={month}
                  onChange={onMonthChange}
                />
                <input
                  className="year"
                  placeholder="2020"
                  onKeyDown={onKeyDown}
                  value={year}
                  onChange={onYearChange}
                />
                <input
                  className="securityCode"
                  placeholder="333"
                  onKeyDown={onKeyDown}
                  value={securityCode}
                  onChange={onSecurityCodeChange}
                />
              </div>
            </div>
          </div>
          <div className="submit">
            <button type="submit">Place Order</button>
          </div>
        </div>
      </form>
    </Paper>
  );
};

export default CreditCard;
