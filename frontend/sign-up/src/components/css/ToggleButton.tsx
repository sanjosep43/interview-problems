import React from 'react';
import {useSpring, animated} from 'react-spring';
import './ToggleButton.css';

export default function ToggleButton() {
  const [isToggled, setIsToggled] = React.useState(false);
  const onToggle = React.useCallback(() => setIsToggled(!isToggled), [isToggled]);
  const {backgroundColor} = useSpring({
    backgroundColor: isToggled ? 'green' : 'red',
    // transform: isToggled ? 'translate3d(0, 15px, 0)' : 'translate3d(0, 15px, 0)',
  });

  console.log(backgroundColor);
  return (
    <div className="outerToggleContainer">
      <div className="toggleContainer">
        <animated.div>
          <div className="toggleSlider" style={{color: backgroundColor}} onClick={onToggle}></div>
        </animated.div>
        <div className="toggleLeftImage"></div>
        <div className="toggleRightImage"></div>
      </div>
    </div>
  );
}
