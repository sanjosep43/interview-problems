import React from 'react';
import './Temperature.css';

const Temperature = () => {
  return (
    <div className="tempContainer">
      <div className="outerCircle">
        <div className="tempBand">
          <div className="innerCircle"></div>
        </div>
      </div>
    </div>
  );
};

export default Temperature;
