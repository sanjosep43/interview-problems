import React from 'react';
import './Calc.css';

const Calc = () => {
  return (
    <div className="calcContainer">
      <div className="display">
        <input disabled value="123" />
      </div>
      <div className="input">1</div>
      <div className="input">2</div>
      <div className="input">3</div>
      <div className="input">+</div>
      <div className="input">4</div>
      <div className="input">5</div>
      <div className="input">6</div>
      <div className="input">-</div>
      <div className="input">7</div>
      <div className="input">8</div>
      <div className="input">9</div>
      <div className="input">/</div>
      <div className="input">C</div>
      <div className="input">0</div>
      <div className="input">=</div>
      <div className="input">*</div>
    </div>
  );
};

export default Calc;
