import React from 'react';
import './Landing.css';

const Landing = () => {
  return (
    <div className="landingContainer">
      Golf at the Dunes
      <img src="images/dunes.jpg" alt="background" />
    </div>
  );
};

export default Landing;
