import React from 'react';
import './SignUp.css';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';

const text =
  'Enter your email address to receive notifications about climate change meetups in your area!';
const emailRegex = /\w+@\w+\.\w+/;

const SignUp = () => {
  const [email, setEmail] = React.useState('');
  const [showSubmittedMessage, setShowSubmittedMessage] = React.useState('');
  const [isValidEmail, setIsValidEmail] = React.useState(true);
  const onSubmit = React.useCallback(() => {
    setShowSubmittedMessage(email);
    setEmail('');
  }, [email]);
  const onEmailChange = React.useCallback((ev) => {
    setEmail(ev.target.value);
    setIsValidEmail(!!ev.target.value.match(emailRegex));
  }, []);
  return (
    <div className="container">
      <p className="text">{text}</p>
      <div className="inputContainer">
        <Input
          className="emailInput"
          placeholder="Email address"
          onChange={onEmailChange}
          value={email}
        />
        <Divider className="divider" orientation="vertical" />
        <IconButton aria-label="submit" onClick={onSubmit} disabled={!isValidEmail}>
          <ChevronRightIcon />
        </IconButton>
      </div>
      {!isValidEmail && (
        <div className="error">
          Error: Email needs to be in in the following format: abc@abc.com
        </div>
      )}
      {!!showSubmittedMessage && (
        <div className="success">Submitted {showSubmittedMessage}!!!!</div>
      )}
    </div>
  );
};

export default SignUp;
