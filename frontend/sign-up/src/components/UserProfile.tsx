import React from 'react';
import './UserProfile.css';

const UserProfile = () => {
  return (
    <div className="profileContainer">
      <div className="profileTop">
        <div className="profilePicture">
          <img src="/images/profile.jpeg" height="100px" width="100px" alt="profile pic" />
          <button>Upload picture</button>
        </div>
        <div className="profileInfo">
          <div>
            <span className="profileInfoTitle">System Administrator</span>
          </div>
          <div>
            <span className="aboutKey">Department:</span>{' '}
            <span className="valueKey">Computer Science</span>
          </div>
          <div>
            <span className="aboutKey">Location:</span> <span className="valueKey">Building 5</span>
          </div>
          <div>
            <span className="aboutKey">Bio:</span> <span className="valueKey">Empty</span>
          </div>
        </div>
      </div>
      <div className="profileBottom">
        <div className="contactInfo">
          <p className="contactInfoTitle">About</p>
          <div>
            <span className="aboutKey">Email:</span>
            <span className="valueKey">abc@ab.com</span>
          </div>
          <div>
            <span className="aboutKey">Mobile phone:</span>
            <span className="valueKey">123-123-4567</span>
          </div>
          <div>
            <span className="aboutKey">Business phone:</span>
            <span className="valueKey">123-123-4567</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
