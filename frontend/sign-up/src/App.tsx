import React from 'react';
import './App.css';
import SignUp from './components/SignUp';
import CreditCard from './components/CreditCard';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import Landing from './components/Landing';
import Calc from './components/Calc';
import UserProfile from './components/UserProfile';
import Temperature from './components/css/Temperature';
import ToggleButton from './components/css/ToggleButton';
import Page404 from './components/css/Page404';

// TODO: react router
function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/sign-up">Sign Up</Link>
            </li>
            <li>
              <Link to="/credit-card">Credit Card</Link>
            </li>
            <li>
              <Link to="/calc">Calc</Link>
            </li>
            <li>
              <Link to="/user-profile">Profile</Link>
            </li>
            <li>
              <Link to="/temperature">Temperature</Link>
            </li>
            <li>
              <Link to="/toggle">Toggle Button</Link>
            </li>
            <li>
              <Link to="/404">Page 404</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/calc">
            <Calc />
          </Route>
          <Route path="/sign-up">
            <SignUp />
          </Route>
          <Route path="/credit-card">
            <CreditCard />
          </Route>
          <Route path="/user-profile">
            <UserProfile />
          </Route>
          <Route path="/temperature">
            <Temperature />
          </Route>
          <Route path="/toggle">
            <ToggleButton />
          </Route>
          <Route path="/404">
            <Page404 />
          </Route>
          <Route path="/">
            <Landing />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
