Array.prototype.isEven = function () {
  if (this.length % 2 === 0) return true;
  return false;
};

console.log([1, 2].isEven());
console.log([1, 2, 3].isEven());
