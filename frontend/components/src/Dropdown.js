import './Dropdown.css';
import React from 'react';

const Dropdown = ({items}) => {
  const [showItems, setShowItems] = React.useState(false);
  const [selectedItem, setSelectedItem] = React.useState('');
  const handleClick = React.useCallback(() => {
    setShowItems(true);
  }, [setShowItems]);
  const handleItemClick = React.useCallback(
    (i) => {
      setShowItems(false);
      setSelectedItem(i);
    },
    [setShowItems, setSelectedItem]
  );

  return (
    <div>
      <input type="text" onClick={handleClick} value={selectedItem} />
      {showItems && (
        <ul className="items-container">
          {items.map((i) => {
            return (
              <li key={i} onClick={() => handleItemClick(i)}>
                {i}
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};

export default Dropdown;
