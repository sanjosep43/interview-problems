import './TicTacToe.css';
import React from 'react';

const EMPTY = '';
const X = 'X';
const O = 'O';
let currentMark = EMPTY;

const getMark = () => {
  if (currentMark === EMPTY) {
    currentMark = X;
    return currentMark;
  } else if (currentMark === X) {
    currentMark = O;
    return currentMark;
  } else if (currentMark === O) {
    currentMark = X;
    return currentMark;
  }
};

const Box = ({updateGame, x, y}) => {
  const [mark, setMark] = React.useState(EMPTY);
  const onClick = React.useCallback(() => {
    if (mark === EMPTY) {
      const m = getMark();
      setMark(m);
      updateGame(m, x, y);
    }
  }, [mark, setMark, updateGame, x, y]);
  let m = '';
  if (mark !== EMPTY) {
    m = mark === X ? 'X' : 'O';
  }
  // mark === X ? 'X' : 'O'
  return (
    <div className="box" onClick={onClick}>
      {m}
    </div>
  );
};

const GridRow = ({length, updateGame, row}) => {
  const boxes = [];
  for (let h = 0; h < length; h++) {
    boxes.push(<Box updateGame={updateGame} x={row} y={h} />);
  }
  return <div className="row">{boxes}</div>;
};

const Grid = ({height, width, updateGame}) => {
  const boxes = [];
  for (let h = 0; h < height; h++) {
    boxes.push(<GridRow length={width} updateGame={updateGame} row={h} />);
  }
  return <div className="container">{boxes}</div>;
};

const TicTacToe = () => {
  const updateGame = React.useCallback((m, x, y) => {
    console.log(m, x, y);
  }, []);

  return <Grid height={3} width={3} updateGame={updateGame} />;
};

export default TicTacToe;
