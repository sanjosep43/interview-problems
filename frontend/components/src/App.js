import './App.css';
import Dropdown from './Dropdown';
import TicTacToe from './TicTacToe';

const items = ['abc', 'def', 'efg', 'hij', 'lmn', 'opq', 'rst', 'uvw', 'xyz'];

function App() {
  return (
    <>
      <TicTacToe />

      <Dropdown items={items} />
    </>
  );
}

export default App;
