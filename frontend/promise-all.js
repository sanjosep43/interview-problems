const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

const promiseAll = async (input) => {
  const output = [];
  const resolved = [];
  for (let i = 0; i < input.length; i++) {
    resolved[i] = false;
  }
  let ran = false;
  let rejected = false;
  let message = '';

  while (true) {
    let allResolved = true;
    for (let i = 0; i < resolved.length; i++) {
      if (resolved[i] === false) {
        allResolved = false;
        break;
      }
    }
    if (allResolved) {
      break;
    }
    if (!ran) {
      input.forEach((p, i) => {
        if (p instanceof Promise) {
          p.then(
            (v) => {
              output[i] = v;
              resolved[i] = true;
            },
            (r) => {
              rejected = true;
              message = r;
            }
          );
        } else {
          output[i] = p;
          resolved[i] = true;
        }
      });
      ran = true;
    }
    if (rejected) break;
    await sleep(0);
  }
  if (rejected) {
    return Promise.reject(message);
  }
  return Promise.resolve(output);
};

const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});
const promise4 = new Promise((resolve, reject) => {
  setTimeout(reject, 1000, 'problem!');
});

promiseAll([promise1, promise2, promise3, promise4]).then(
  (values) => {
    console.log(values);
  },
  (error) => {
    console.log(error);
  }
);
// expected output: Array [3, 42, "foo"]
