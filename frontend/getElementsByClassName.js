function getElementsByClassName(classNames) {
  const results = [];
  helper(document.children[0], classNames, results);
  return results;
}

function helper(node, classNames, results) {
  for (let i = 0; i < node.classList.length; i++) {
    if (classNames.includes(node.classList[i])) {
      results.push(node);
      break;
    }
  }
  for (let i = 0; i < node.children.length; i++) {
    helper(node.children[i], classNames, results);
  }
}
