function JSONStringify(obj) {
  if (typeof obj === 'number') {
    return obj.toString();
  }
  if (typeof obj === 'string') {
    return obj.toString();
  }
  if (Array.isArray(obj)) {
    let str = '[';
    for (let i = 0; i < obj.length; i++) {
      str += JSONStringify(obj[i]) + ',';
    }
    return str.substring(0, str.length - 1) + ']';
  }
  if (typeof obj === 'object') {
    let str = '{';
    const keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
      if (typeof obj[keys[i]] === 'function') {
        continue;
      }
      str += keys[i] + ': ' + JSONStringify(obj[keys[i]]) + ',';
    }
    return str.substring(0, str.length - 1) + '}';
  }
  return 'error';
}

console.log(JSON.stringify(3), JSONStringify(3));
console.log(JSON.stringify(3), JSONStringify('abc'));
console.log(JSON.stringify(3), JSONStringify(['abc', 3]));
console.log(JSON.stringify(3), JSONStringify(['abc', 3, {a: 'abc', b: 3}]));
