// This is the text editor interface. 
// Anything you type or change here will be seen by the other person in real time.

const data = [
  {
    city: 'LA',
    country: 'US',
    metric: 671
  },
  {
    city: 'LA',
    country: 'US',
    metric: 354
  },
  {
    city: 'LA',
    country: 'CANADA',
    metric: 539
  },
  {
    city: 'NYC',
    country: 'US',
    metric: 710
  },
  {
    city: 'LA',
    country: 'CANADA',
    metric: 435
  },
  {
    city: 'SF',
    country: 'CANADA',
    metric: 395
  }
];

//groupBy('city');
const groupedByCity = {
  LA: [
    {city: 'LA', country: 'US', metric: 671},
    {city: 'LA', country: 'US', metric: 354}
  ],
  TORONTO: [
    {city: 'TORONTO', country: 'CANADA', metric: 539},
    {city: 'TORONTO', country: 'CANADA', metric: 435}
  ],
  NYC: [{city: 'NYC', country: 'US', metric: 710}],
  VANCOUVER: [{city: 'VANCOUVER', country: 'CANADA', metric: 395}]
};

function groupBy(key) {
    // let sortedData = data.sort((a, b) => {
    //     if(a[key] == b[key]) {
    //         return 0;
    //     } else if(a[key] < b[key]) {
    //         return 1;
    //     } else if(a[key] > b[key]) {
    //         return 1;
    //     }
    // });
    // console.log(sortedData);
    let keys = data.map(x => x[key]);
    // console.log(keys);
    let output = {};
    for(let i = 0; i < keys.length; i++){
        if(!output[keys[i]]){
            // output[keys[i]] = [];
            output[keys[i]] = data.filter(x => x[key] === keys[i]);
            // for(let x = 0; x < f.length; f++)
            // console.log('F:', f);
        }
    }
    return output; 
}

function groupByRewrite(key){
    let output = {};
    for(let i = 0; i < data.length; i++){
      if(!output[data[i][key]]) {
         output[data[i][key]] = [];
      } 
      output[data[i][key]].push(data[i])
    }
    return output;
}

const groupedByCountryCity = {
  US: {
    LA: [
      {city: 'LA', country: 'US', metric: 671},
      {city: 'LA', country: 'US', metric: 354}
    ],
    NYC: [{city: 'NYC', country: 'US', metric: 710}]
  },
  CANADA: {
    TORONTO: [
      {city: 'TORONTO', country: 'CANADA', metric: 539},
      {city: 'TORONTO', country: 'CANADA', metric: 435}
    ],
    VANCOUVER: [{city: 'VANCOUVER', country: 'CANADA', metric: 395}]
  }
};

function groupByTwo(array) {
    for(let i = 0; i < array.length; i++){
        let result = groupByRewrite(array[i]);
        console.log(result);
    }
}


console.log(groupByRewrite("city"));
// groupByTwo(['country', 'city'])
//console.log(groupBy("city"));
// console.log(groupByRewrite("city"));
// a = ["a", "c", "b"]
// console.log(a.sort());
// console.log(a.sort( (a, b) => console.log(a, b) ));

