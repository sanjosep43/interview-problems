// Given a positive integer, find the largest number that could be
// generated by swapping only two digits at most once.
/*
Input: 2736
Output : 7236
Explanation:
If we swap the number 2 and the number
7 then the generated number would be
the largest number.

Input : 432
Output : 432
Explanation:
Here, no swap is required. The given
number is already largest.
*/

function largestNumberSwap(input) {
  let numbers = [];
  let num = input;
  while(num > 0) {
    let n = num % 10;
    numbers.unshift(n);
    num = parseInt(num / 10);
  }
  //console.log(numbers);
  if(numbers.length === 1) {
    return numbers[0];
  }
  let largestIdx = 0;
  let largestNumber = 0;
  for(let i = 0; i < numbers.length; i++){
    //console.log('--', numbers[i], largestNumber);
    if(numbers[i] >= largestNumber) {
      largestNumber = numbers[i];
      largestIdx = i;
    }
  }
  //console.log('3:', largestIdx, largestNumber)
  let firstPositionNum = numbers[0];
  numbers[largestIdx] = firstPositionNum;
  numbers[0] = largestNumber;
  //console.log('1:', numbers);
  let numStr = numbers.join('');
  //console.log('2:', numStr);
  let newNum = parseInt(numStr);
  return newNum > input ? newNum : input;
}

console.log(largestNumberSwap(4));
console.log(largestNumberSwap(43));
console.log(largestNumberSwap(437));
console.log(largestNumberSwap(2736));
console.log(largestNumberSwap(2776));
