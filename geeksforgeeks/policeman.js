// http://www.geeksforgeeks.org/policemen-catch-thieves/
/*
Input : arr[] = {'P', 'T', 'T', 'P', 'T'},
            k = 1.
Output : 2.
Here maximum 2 thieves can be caught, first
policeman catches first thief and second police-
man can catch either second or third thief.

Input : arr[] = {'T', 'T', 'P', 'P', 'T', 'P'}, 
            k = 2.
Output : 3.

Input : arr[] = {'P', 'T', 'P', 'T', 'T', 'P'},
            k = 3.
Output : 3.
*/


function policeAndTheives(input, k) {
  let caught = 0;
  let police = [];
  for(let i = 0; i < input.length; i++){
    police.push(false);
  }
  for(let i = 0; i < input.length; i++){
    if(input[i] === 'T') {
      let start = (i - k) <= 0 ? 0 : i - k;
      for(let x = start; x < (i + k); x++){
        if(input[x] === 'P' && !police[x]) {
          caught++;
          police[x] = true;
        }
      }
      start = i + 1 >= input.length ? i : i + 1;
      for(let x = start; x < (i + k); x++) {
        if(input[x] === 'P' && !police[x]) {
          caught++;
          police[x] = true;
        }
      }
    }
  }
  return caught;
}

console.log(policeAndTheives(['P', 'T', 'T', 'P', 'T'], 1));
console.log(policeAndTheives(['T', 'T', 'P', 'P', 'T', 'P'], 2));
console.log(policeAndTheives(['P', 'T', 'P', 'T', 'T', 'P'], 3));