// http://www.geeksforgeeks.org/rearrange-positive-negative-numbers-using-inbuilt-sort-function/

function sort(input) {
  let negIdx = 0;
  for(let i = 0; i < input.length; i++){
    if(input[i] < 0){
      let temp = input[i];
      for(let shiftIdx = i-1; shiftIdx >= negIdx; shiftIdx--){
        input[shiftIdx+1] = input[shiftIdx];
      }
      input[negIdx] = temp;
      negIdx++;
    }
  }
  return input;
}

console.log(sort([12,11,-13,-5,6,-7,5,-3,-6]));
console.log(sort([-12,11,0,-5,6,-7,5,-3,-6]));
