

function doesIntersect(l1, l2) {
  let rightMostL1 = l1.p1;
  let leftMostL1 = l1.p2;
  if(l1.p1.x < l1.p2.x) {
    rightMostL1 = l1.p2;
    leftMostL1 = l1.p1;
  }
  let pointsOnL1 = [];
  if(rightMostL1.x > leftMostL1.x){
    for(let x = rightMostL1.x, y = rightMostL1.y;
        x < rightMostL1.x-leftMostL1.x && y < rightMostL1.y-leftMostL1.y;
        x--, y--) {
      pointsOnL1.push({x: x, y: y});
    }
  } else {
    for(let x = rightMostL1.x, y = rightMostL1.y; x < rightMostL1.x-leftMostL1.x; x--, y--) {
      pointsOnL1.push({x: x, y: y});
    }
  }

  let rightMostL2 = l2.p2;
  let leftMostL2 = l2.p1;
  if(l2.p1.x < l2.p2.x){
    rightMostL2 = l2.p1;
    leftMostL2 = l2.p2;
  }
}

let result = doesIntersect(
  {p1:{x: 0, y: 0}, p2: {x:2, y: 2}},
  {p1:{x: 0, y: 2}, p2: {x:2, y: 0}}
);
console.log(result);