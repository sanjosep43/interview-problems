function riverSizes(matrix) {
  const visited = [];
  const width = matrix[0].length;
  for (let i = 0; i < matrix.length; i++) {
    visited.push([]);
    for (let x = 0; x < width; x++) {
      visited[i].push(false);
    }
  }
  const riverLengths = [];
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < matrix.length; y++) {
      if (matrix[y][x] === 1 && visited[y][x] === false) {
        const len = 1 + getRiverLength(matrix, visited, x, y);
        riverLengths.push(len);
      }
    }
  }
  return riverLengths;
}

function getRiverLength(matrix, visited, x, y) {
  visited[y][x] = true;
  let len = 0;
  // find the way the river goes
  if (isPartOfRiver(matrix, visited, x + 1, y)) {
    len += 1 + getRiverLength(matrix, visited, x + 1, y);
  }
  if (isPartOfRiver(matrix, visited, x - 1, y)) {
    len += 1 + getRiverLength(matrix, visited, x - 1, y);
  }
  if (isPartOfRiver(matrix, visited, x, y + 1)) {
    len += 1 + getRiverLength(matrix, visited, x, y + 1);
  }
  if (isPartOfRiver(matrix, visited, x, y - 1)) {
    len += 1 + getRiverLength(matrix, visited, x, y - 1);
  }
  return len;
}

function isPartOfRiver(matrix, visited, x, y) {
  if (x >= matrix[0].length || x < 0 || y >= matrix.length || y < 0) {
    return false;
  }
  if (visited[y][x] === true) {
    return false;
  }
  visited[y][x] = true;
  if (matrix[y][x] === 0) {
    return false;
  }
  return true;
}

// function riverHelper(matrix, visited, x, y) {
//   if(x > matrix[0].length || x < 0 || y > matrix.length || y < 0) {
//     return 0;
//   }
//   if(visited[y][x] === true) {
//     return 0;
//   }
//   visited[y][x] = true;
//   if(matrix[y][x] === 0) {
//     return 0;
//   }
//   if(matrix[y][x] === 1) {
//     return 1
//   }
// }

// Do not edit the line below.
exports.riverSizes = riverSizes;

const result = riverSizes([
  [1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0],
  [1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0],
  [0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1],
  [1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0],
  [1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1],
]);

console.log('correct answer: [2, 1, 21, 5, 2, 1]');
console.log(result);
