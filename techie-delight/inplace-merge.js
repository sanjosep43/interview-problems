
//in place merge of 2 sorted arrays
//a = [1,2] b = [3, 4, 5]
function merge(a, b) {
    let lastIdx = 0;
    let length = a.length > b.length ? b.length : a.length;
    for(let i = 0; i < length; i++) {
        if(b[i] <= a[i]) {
            a.splice(i, 0, b[i]);
            lastIdx++;
        }
    }
    return a.concat(b.splice(lastIdx));
}
