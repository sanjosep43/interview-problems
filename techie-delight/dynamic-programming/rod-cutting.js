const assert = require('assert').strictEqual;

function rodCutting(prices, n) {
  return helper(prices, n, prices[n]);
}

function helper(prices, n, sum) {
  if (n === 1) {
    return prices[n] + sum;
  }
  helper(prices, n - 1, sum + prices[n - 1]);
}

assert(rodCutting([1, 5, 8, 9, 10, 17, 17, 20], 4), 10);