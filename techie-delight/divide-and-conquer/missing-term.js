const assert = require('assert').strictEqual;

function missingTerm(input) {
  const diff = input[1] - input[0];
  return helper(input, diff);
}

function helper(input, k) {
  if (input.length === 2) {
    if (input[1] - input[0] !== k) {
      return input[0] + k;
    }
    return null;
  }
  const left = helper(input.slice(0, Math.ceil(input.length / 2)), k);
  const right = helper(input.slice(Math.floor(input.length / 2), input.length), k);
  return left ? left : right;
}

assert(missingTerm([1, 3, 7]), 5);
assert(missingTerm([5, 7, 9, 11, 15]), 13);
assert(missingTerm([1, 4, 7, 13, 16]), 10);