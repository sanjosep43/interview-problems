# Function to check if sublist with 0 sum is present
# in the given list or not
def zeroSumSublist(A):

    # create an empty set to store sum of elements of each
    # sublist A[0..i] where 0 <= i < len(A)
    s = set()

    # insert 0 into set to handle the case when sublist with
    # 0 sum starts from index 0
    s.add(0)

    sum = 0

    # traverse the given list
    for i in A:

        # sum of elements so far
        sum += i

        # if sum is seen before, we have found a sublist with 0 sum
        if sum in s:
            return True

        # insert sum so far into set
        s.add(sum)

    # we reach here when no sublist with 0 sum exists
    return False


if __name__ == '__main__':

    #A = [4, -6, 3, -1, 4, 2, 7]
    A = [-3, -1, 0, 2, 4, 4]

    if zeroSumSublist(A):
        print("Sublist exists")
    else:
        print("Sublist do not exist")
