const assert = require('assert').strictEqual;

function checkSubarrayZero(input) {
  let currentSum = 0;
  let hash = new Set();
  hash.add(0);
  for (let i = 0; i < input.length; i++) {
    if (input[i] === 0) {
      continue;
    }
    currentSum += input[i];
    if (hash.has(currentSum)) {
      return true;
    } else {
      hash.add(currentSum);
    }
  }
  return false;
}

/*
4 2 -3 -1 0 4
-3 -1 0 2 4 4
-1 -3 -4
2 4 6 8 10
*/
assert(checkSubarrayZero([4, 2, -3, -1, 0, 4]), true);
assert(checkSubarrayZero([-3, -1, 0, 2, 4, 4]), false);
