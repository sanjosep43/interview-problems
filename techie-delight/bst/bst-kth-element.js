const assert = require('assert').strictEqual;
const createBinaryTree = require('../binary-tree/create-binary-tree');

function findKthSmallestAndLargest(root, k) {
  const smallestResults = [root.val];
  getSmallestValues(root.left, smallestResults);
  console.log(smallestResults);
  const largestResults = [root.val];
  getSmallestValues(root.right, largestResults);
  console.log(largestResults);
  return [];
}

function getSmallestValues(node, results) {
  if (!node) {
    return;
  }
  getSmallestValues(node.left, results);
  results.push(node.val);
  getSmallestValues(node.right, results);
}

let t0 = createBinaryTree([1, 2, 3]);
assert(findKthSmallestAndLargest(t0, 1), [1, 3]);

let t1 = createBinaryTree([2, 1, 3]);
assert(findKthSmallestAndLargest(t1, 1), [1, 3]);

let t2 = createBinaryTree([1, 20, 30, 40, 50, 2, 3, 4, 5]);
assert(findKthSmallestAndLargest(t2, 2), [2, 40]);

