function matchQues(s, m) {
  let si = 0;
  for(let mi = 0; mi < m.length; mi++) {
      if(m[mi] === '?') {
        ++si;
      } else if (m[mi] !== s[si]) {
          return false;
      }
  }
  return true;
}

