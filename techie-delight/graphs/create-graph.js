
function createGraph(edges) {
  const graph = {};
  for (let i = 0; i < edges.length; i++) {
    if (!graph[edges[i][0]]) {
      graph[edges[i][0]] = edges[i][1] !== undefined ? [edges[i][1]] : [];
    } else {
      graph[edges[i][0]].push_back(edges[i][1]);
    }
  }
  return graph;
}

module.exports = createGraph;

// console.log(createGraph([[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0]]))