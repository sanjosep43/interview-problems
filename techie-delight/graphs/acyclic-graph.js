const assert = require('assert').strictEqual;
const createGraph = require('./create-graph');

function dfs(graph, node, found) {
  if (graph[node].length === 0) {
    return false;
  }
  if (found.indexOf(graph[node])) {
    return true;
  }
  found.push(graph[node]);
  for (let i = 0; i < graph[node].length; i++) {
    if (dfs(graph, graph[node][i], found)) {
      return true;
    }
  }
  return false;
}

const acyclicGraph = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 0]];
const found = [];
assert(dfs(createGraph(acyclicGraph), 0, found), true);
