const createList = require('./create-list');
const assert = require('assert').strictEqual;

function alternateHighLows(list) {

}

module.exports = alternateHighLows;

assert(
  alternateHighLows(
    createList([1, 2, 3, 4, 5, 6, 7])
  ),
  [1, 3, 2, 5, 4, 7, 6]
);