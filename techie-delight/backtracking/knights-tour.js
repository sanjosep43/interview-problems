
function allPossibleKnightsTour() {
  const board = [];
  for (let row = 0; row < 8; row++) {
    board[row] = [];
    for (let col = 0; col < 8; col++) {
      board[row][col] = 0;
    }
  }
  //knight move: 4 directions -> 3, then 2 directions: 2
  //knight move: 4 directions -> 2, then 2 directions: 3
  function getAllPossibleMoves(b, row, col) {
    const results = [];
    if (checkIfValidMove(b, row + 1, col + 2)) {
      results.push([row + 1, col + 2]);
    }
    if (checkIfValidMove(b, row + 1, col - 2)) {
      results.push([row + 1, col - 2]);
    }
    if (checkIfValidMove(b, row + 1, col - 2)) {
      results.push([row + 1, col - 2]);
    }
    if (checkIfValidMove(b, row + 1, col - 2)) {
      results.push([row + 1, col - 2]);
    }
    return results;
  }
  function checkIfValidMove(b, row, col) {
    if (b !== 0) {
      return false;
    }
    if (row < 0 || row > 7) {
      return false;
    }
    if (col < 0 || col > 7) {
      return false;
    }
    return true;
  }
  function knightsHelper(row, col) {

  }
  knightsHelper(0, 0)
}

allPossibleKnightsTour();