
function swap(s, a, b) {
  let t = s[a];
  s[a] = s[b];
  s[b] = t;
}

function permute(s, i, len, results) {
  if(i === len-1) {
    console.log('adding:', s)
    results.push(s.slice(0));
    console.log('results:', results);
    return;
  }

  for(let x = i; x < len; x++) {
    swap(s, i, x);
    permute(s, i+1, len, results);
    swap(s, i, x);
  }
}

let results = [];
permute(['a', 'b', 'c'], 0, 3, results);;
