
// a = [1, 5, 7]
// b = [1, 2, 8, 9]
function merge(a, b) {
    let shortest = a.length > b.length ? b.length : a.length;
    let list = [];
    let aIdx = 0;
    let bIdx = 0;
    for(; aIdx < shortest && bIdx < shortest; ) {
        if(a[aIdx] == b[bIdx]) {
            list.push(a[aIdx]);
            list.push(b[bIdx]);
            ++aIdx;
            ++bIdx;
        } else if (a[aIdx] < b[bIdx]) {
            list.push(a[aIdx]);
            ++aIdx;
        } else {
            list.push(b[bIdx]);
            ++bIdx;
        }
    }
    if(aIdx < a.length) {
      list = list.concat(a.slice(aIdx, a.length-aIdx));
    } else if( bIdx < b.length) {
      list = list.concat(b.slice(aIdx, b.length-bIdx));
    }
    return list;
}
