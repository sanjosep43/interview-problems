const createTree = require('../binary-tree/create-binary-tree');

function diagonalTraversal(root) {
  const queue = [];
  queue.push(root);
  while (queue.length > 0) {
    let current = queue.shift();
    while (current) {
      console.log(current.val);
      if (current.left) {
        queue.push(current.left);
      }
      current = current.right;
    }
  }
}

const t1 = createTree([10, 20, 30, 40, 9, 13, 23, 33]);

diagonalTraversal(t1);