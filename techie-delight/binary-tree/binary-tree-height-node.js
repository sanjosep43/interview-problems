
function getHeightOfNode(root, val, h=0) {
  if(!root) {
    return false;
  }
  if(root.val === val) {
    return {result: true, height: h};
  }
  h = h + 1;
  const r = getHeightOfNode(root.left, val, h);
  if(!r) {
    return getHeightOfNode(root.right, val, h);
  }
  return r;
}

module.exports = getHeightOfNode;