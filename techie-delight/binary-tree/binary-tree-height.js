
function getHeight(node, h=0) {
  if(!node) {
    return h;
  }
  h = h + 1;
  let h1 = getHeight(node.left, h);
  let h2 = getHeight(node.right, h);
  return h1 > h2 ? h1 : h2;
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7,
  left: {
    val: 8
  }
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};


// console.log(getHeight(root));

module.exports = getHeight;
