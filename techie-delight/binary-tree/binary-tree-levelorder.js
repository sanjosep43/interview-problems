
function levelorder(n, results) {
  let q = [];
  q.push(n);
  results.push(n.val);
  while(q.length > 0) {
    let l = q.length;
    while(l--) {
      let e = q.shift();
      if(e.left) {
        q.push(e.left);
        results.push(e.left.val);
      }
      if(e.right) {
        q.push(e.right);
        results.push(e.right.val);
      }
    }
  }
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

// let results = [];
// levelorder(root, results);
// console.log(results);

module.exports = levelorder;