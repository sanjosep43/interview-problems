
const getHeight = require('./binary-tree-height');

function diameter(node) {
  if(!node) {
    return 1;
  }
  const lh = getHeight(node.left);
  const rh = getHeight(node.right);
  const dia = lh + rh + 1;
  const dialeft = diameter(node.left);
  const diaright = diameter(node.right);
  if(dia >= dialeft && dia >= diaright) {
    return dia;
  }
  if(dialeft >= dia && dialeft >= diaright) {
    return dialeft;
  }
  return diaright;
}

module.exports = diameter;
