
function next(root, node) {
  console.log('finding:', node)
  let q = [];
  q.push(root);
  while(q.length > 0) {
    let len = q.length;
    for(let i = 0; i < q.length; i++) {
     if(q[i] === node) {
       console.log('here')
       return q[i+1];
     }
    }
    while(len--) {
      let c = q.shift();
      if(c.left) q.push(c.left);
      if(c.right) q.push(c.right);
      console.log(q);
    }
  }
  console.log('here2')
}

let t1 = {
  val: 1,
  right: {},
  left: {}
};

//console.log(next(t1, t1));

let r1 = {val: 3};
let l1 = {val: 2};
let t2 = {
  val: 1,
  right: r1,
  left: l1
};

//console.log(next(t2, l1));

let l2 = { val: 4 };
let r2 = { val: 5 };
let r3 = { val: 6 };
l1 = {
  val: 2,
  left: l2,
  right: r3
}
r1 = {
  val: 3,
  right: r2
};

let t3 = {
  val: 1,
  left: l1,
  right: r1
};

console.log(next(t3, l2));
