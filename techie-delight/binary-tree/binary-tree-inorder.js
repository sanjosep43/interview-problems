
function inorder(n, results) {
  if(!n) {
    return;
  }
  inorder(n.left, results);
  results.push(n.val);
  inorder(n.right, results);
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

let results = [];
inorder(root, results);
console.log(results);


