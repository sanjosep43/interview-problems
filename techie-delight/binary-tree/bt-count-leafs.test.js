const createBinaryTree = require('./create-binary-tree');
const countLeafs = require('./bt-count-leafs');

const t1 = createBinaryTree([8, 7, 6, 9, 12, 15, 10]);
/*
               8
            7 .   9
          6 .  .    12
                  10    15
*/

console.log(countLeafs(t1));