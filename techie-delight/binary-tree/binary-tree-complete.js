
function isComplete(n) {
  if(!n) {
    return true;
  }
  if(!n.left && n.right) {
    return false;
  }
  if(n.left && n.right) {
    return true;
  }
  return isComplete(n.left) || isComplete(n.right);
}

module.exports = isComplete;
