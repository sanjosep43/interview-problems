
function setNodeInfo(n, h=0) {
  if(!n) {
    return;
  }
  n.h = h;
  if(n.left) n.left.p = n;
  if(n.right) n.right.p = n;
  h = h + 1;
  setNodeInfo(n.left, h);
  setNodeInfo(n.right, h);
}

function findNodeHeight(n, n1) {
  if(!n) {
    return null;
  }
  if(n === n1) {
    return n;
  }
  let result = findNodeHeight(n.left, n1);
  if(!result) {
    return findNodeHeight(n.right, n1);
  }
  return result;
}

function findAllNodesHeightOf(n, h, result) {
  if(!n) {
    return;
  }
  if(n.h > h) {
    return;
  }
  if(n.h == h) {
    result.push(n);
  }
  findAllNodesHeightOf(n.left, h, result);
  findAllNodesHeightOf(n.right, h, result);
}

function printCousins(n, n1) {
  setNodeInfo(n);
  let node = findNodeHeight(n, n1);
  let nodes = [];
  findAllNodesHeightOf(n, node.h, nodes);
  return nodes.filter(x => x.p !== node.p);
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

console.log(printCousins(root, l2l));
