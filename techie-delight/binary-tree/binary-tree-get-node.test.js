
const getNodeByValue = require('./binary-tree-get-node');
const createTree = require('./create-binary-tree');

const a1 = createTree([1,2,3,4,5]);
const a2 = createTree([5,4,3,2,1]);
const a3 = createTree([5,3,4,1,2]);
const a4 = createTree([1,4,3,5,2]);

console.log(getNodeByValue(a1, 4));
console.log(getNodeByValue(a2, 5));
console.log(getNodeByValue(a3, 1));
console.log(getNodeByValue(a4, 3));
console.log(getNodeByValue(a4, 7));
