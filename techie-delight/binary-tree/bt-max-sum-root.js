const assert = require('assert').strictEqual;
const createBinaryTree = require('./create-binary-tree');

function maxSumRootToLeaf(root) {
  if (!root) {
    return 0;
  }
  const results = [];
  helper(root, results, 0);
  results.sort((a, b) => {
    if (a > b) return -1;
    else if (a < b) return 1;
    return 0;
  });
  console.log(results);
  return results[0];
}

function helper(node, results, totalSoFar) {
  if (!node) {
    return;
  }
  totalSoFar += node.val;
  if (!node.right && !node.left) {
    results.push(totalSoFar);
  }
  //node.totalSoFar += node.val;
  helper(node.left, results, totalSoFar);
  helper(node.right, results, totalSoFar);
}

let t0 = createBinaryTree([1, 2, 3]);
assert(maxSumRootToLeaf(t0), 6);

let t1 = createBinaryTree([2, 1, 3]);
assert(maxSumRootToLeaf(t1), 5);

let t2 = createBinaryTree([1, 20, 30, 40, 50, 2, 3, 4, 5]);
assert(maxSumRootToLeaf(t2), 141);