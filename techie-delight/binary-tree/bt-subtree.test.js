const createBinaryTree = require('./create-binary-tree');
const isSubTree = require('./bt-subtree');
const find = require('./bt-find');
const t1 = createBinaryTree([8, 7, 6, 9, 12, 15, 10]);
/*
               8
            7 .   9
          6 .  .    12
                  10    15
                  */

const t2 = find(t1, 12);
console.log(isSubTree(t1, t2));
console.log(isSubTree(t1, find(t1, 1)));
console.log(isSubTree(t2, t1));