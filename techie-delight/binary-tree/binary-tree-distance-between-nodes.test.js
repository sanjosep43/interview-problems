
const distanceBetweenNodes = require('./binary-tree-distance-between-nodes');
const createBinaryTree = require('./create-binary-tree');

const t1 = createBinaryTree([1,2,3,4,5,6,7]);
const t2 = createBinaryTree([7,6,5,4,3,2,1]);
const t3 = createBinaryTree([5,3,2,4,1,6,7]);

console.log(distanceBetweenNodes(t1,1,7));
console.log(distanceBetweenNodes(t1,1,3));
console.log(distanceBetweenNodes(t1,3,7));
console.log(distanceBetweenNodes(t2,1,7));
console.log(distanceBetweenNodes(t2,1,3));
console.log(distanceBetweenNodes(t2,3,7));
console.log(distanceBetweenNodes(t3,2,7));
console.log(distanceBetweenNodes(t3,4,7));
