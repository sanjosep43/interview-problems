const createBinaryTree = require('./create-binary-tree');
const bfs = require('./binary-tree-bfs');
const assert = require('assert').strict;

const t1 = createBinaryTree([8, 7, 6, 9, 12, 15, 10]);

const result = [];
bfs(t1, result);
console.log(result);
assert(result.length === 7);