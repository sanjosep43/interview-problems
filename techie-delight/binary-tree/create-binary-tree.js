
function insert(node, val) {
  if(!node.val) {
    node.val = val;
    return;
  }
  if(val < node.val) {
    if(!node.left) {
      node.left = {};
    }
    insert(node.left, val);
    return;
  } else {
    if(!node.right) {
      node.right = {};
    }
    insert(node.right, val);
    return;
  }
}

module.exports = (a) => {
  let root = {};
  a.forEach(val => {
    insert(root, val);
  });
  return root;
}