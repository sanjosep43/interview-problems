
const createTree = require('./create-binary-tree');
const levelOrderTraversal = require('./binary-tree-levelorder');

let t1 = createTree([1,20,30,40,50,2,3,4,5]);
let results1 = [];
levelOrderTraversal(t1, results1);
console.log(results1);