
function leftView(node, r=[]) {
  if(!node) {
    return;
  }
  if(node.left) {
    r.push(node.left.val);
  }
  leftView(node.left, r);
  leftView(node.right,r);
};

module.exports = leftView;
