
function preorder(n, results) {
  if(!n) {
    return;
  }
  results.push(n.val);
  preorder(n.left, results);
  preorder(n.right, results);
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

let results = [];
preorder(root, results);
console.log(results);
