
const nodeExists = require('./binary-tree-node-exists');
const getHeightOfNode = require('./binary-tree-height-node');

function distanceBetweenNodes(root, n1, n2) {
  if(root.val === n1 && nodeExists(root, n2)) {
    return getHeightOfNode(root, n2).height;
  }
  if(root.val === n2 && nodeExists(root, n1)) {
    return getHeightOfNode(root, n1).height;
  }
  if(nodeExists(root.left, n1) && nodeExists(root.left, n2)) {
    let n1Height = getHeightOfNode(root.left, n1).height;
    let n2Height = getHeightOfNode(root.left, n2).height;
    return Math.abs(n1Height - n2Height);
  }
  if(nodeExists(root.right, n1) && nodeExists(root.right, n2)) {
    let n1Height = getHeightOfNode(root.right, n1).height;
    let n2Height = getHeightOfNode(root.right, n2).height;
    return Math.abs(n1Height - n2Height);
  }
  if(nodeExists(root.left, n1) && nodeExists(root.right, n2)) {
    let n1Height = getHeightOfNode(root.left, n1).height;
    let n2Height = getHeightOfNode(root.right, n2).height;
    return n1Height + n2Height;
  }
  if(nodeExists(root.left, n2) && nodeExists(root.right, n1)) {
    let n1Height = getHeightOfNode(root.right, n1).height;
    let n2Height = getHeightOfNode(root.left, n2).height;
    return n1Height + n2Height;
  }
  return 0;
}

module.exports = distanceBetweenNodes;