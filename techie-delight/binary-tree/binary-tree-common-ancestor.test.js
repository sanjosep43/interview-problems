const createBinaryTree = require('./create-binary-tree');
const getCommonAncestor = require('./binary-tree-common-ancestor');

const t1 = createBinaryTree([8, 7, 6, 9, 12, 15, 10]);
//console.log(t1);
/*
               8
            7 .   9
          6 .  .    12
                  10    15
                  */
console.log(getCommonAncestor(t1, 15, 6));