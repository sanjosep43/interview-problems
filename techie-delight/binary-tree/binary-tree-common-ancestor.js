
function getCommonAncestor(root, val1, val2) {
  const parentsOfVal1 = [root];
  const parentsOfVal2 = [root];
  let parent = root;
  while(parent) {
    let p = getParent(
      parent, 
      parentsOfVal1.length === 0 ? val1 : parentsOfVal1[parentsOfVal1.length-1],
      parent === root ? null : parent);
    parentsOfVal1.push(p);
    parent = p;
  } 
  parent = root;
  while(parent) {
    let p = getParent(
      parent, 
      parentsOfVal2.length === 0 ? val2 : parentsOfVal2[parentsOfVal2.length-1],
      parent);
    parentsOfVal2.push(p);
    parent = p;
  }
  console.log(parentsOfVal1);
  console.log(parentsOfVal2);

  if(parentsOfVal1.length === parentsOfVal2.length) {
    return parentsOfVal1[0]
  }
  return parentsOfVal1.length > parentsOfVal2 ?
    parentsOfVal1[0] : parentsOfVal2[0];
}

function getParent(node, val, parent) {
  if(!node) {
    return null;
  }
  if(node.val === val) {
    return parent;
  }
  return getParent(node.left, val, node) ||
         getParent(node.right, val, node);
}

// function checkIfValExists(node, val) {
//   if(!node) {
//     return false;
//   }
//   if(node.val === val) {
//     return true;
//   }
//   return checkIfValExists(node.left, val) || checkIfValExists(node.right, val);
// }

module.exports = getCommonAncestor;

/*
  root has both nodes..YES
  check left subtree if YES, go down left..if NO check right
  keep going down a branch till you find one of the nodes or till you don't find it

               8
            7 .   9
          6 .  .    12
                  10    15

          8 & 6
          getparent of 8 -> 7
          getparent of 6 -> 3

          getparent of 3 -> 1
          getparent of 1 -> null

          getparent of 7 -> 3
          getparent of 3 -> 1
          getparent of 1 -> null
*/