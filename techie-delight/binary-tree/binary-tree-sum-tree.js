

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 11,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 16,
    left: r2l,
    right: r2r
}
let root = {
    val: 28,
    left: l1,
    right: r1
};

convertToSumTree(root);
console.log(root);
