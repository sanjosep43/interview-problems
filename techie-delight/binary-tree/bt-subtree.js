
function isSubTree(mainTree, subTree) {
  if (!mainTree || !subTree) {
    return false;
  }
  const node = find(mainTree, subTree);
  if (!node) {
    return false;
  }
  return isTreeMatch(node, subTree);
}

function find(node1, node2) {
  if (!node1) {
    return null;
  }
  if (node1.val === node2.val) {
    return node1;
  }
  return find(node1.left, node2) || find(node1.right, node2);
}

function isTreeMatch(subTree, mainTree) {
  if (!subTree && !mainTree) {
    return true;
  }
  if (!subTree && mainTree) {
    return false;
  }
  if (subTree.val !== mainTree.val) {
    return false;
  }
  return isTreeMatch(subTree.left, mainTree.left) ||
    isTreeMatch(subTree.right, mainTree.right);

}

module.exports = isSubTree;