
function sumTreeHelper(n) {
  if(n.val === 0){
    return 0;
  }
  return n.val + sumTreeHelper(n.left) + sumTreeHelper(n.right);
}

function isSumTree(n) {
  if(!n) {
    return false;
  }
  let leftSum = sumTreeHelper(n.left);
  let rightSum = sumTreeHelper(n.right);
  if((leftSum + rightSum) === n.val) {
    return isSumTree(n.left) || isSumTree(n.right);
  } else {
    return false;
  }
}

let l2l = {
    val: 0
}
let l2r = {
    val: 0
}
let r2l = {
    val: 0
}
let r2r = {
    val: 0
}
let l1 = {
    val: 11,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 16,
    left: r2l,
    right: r2r
}
let root = {
    val: 27,
    left: l1,
    right: r1
};

console.log(isSumTree(root));
//console.log(sumTreeHelper(root));
