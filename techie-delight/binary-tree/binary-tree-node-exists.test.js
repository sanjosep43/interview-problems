
const nodeExistsByVal = require('./binary-tree-node-exists');
const createTree = require('./create-binary-tree');

const a1 = createTree([1,2,3,4,5]);
const a2 = createTree([5,4,3,2,1]);
const a3 = createTree([5,3,4,1,2]);
const a4 = createTree([1,4,3,5,2]);

console.log(nodeExistsByVal(a1, 0));
console.log(nodeExistsByVal(a1, 5));
console.log(nodeExistsByVal(a1, 3));