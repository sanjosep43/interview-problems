
function getNodeByValue(node, val) {
  if(!node) {
    return null;
  }
  if(node.val === val){
    return node;
  }
  return getNodeByValue(node.left, val) ||
         getNodeByValue(node.right, val);
}

module.exports = getNodeByValue;