function count(n) {
  if(!count) {
    return 0;
  }
  return 1 + count(n.left) + count(n.right);
}

function traverse(n) {
  let c = count(n);
  let queue = [];
  queue.push(n.left);
  queue.push(n.right);
  let index = 0;
  while(true) {
    let n1 = queue[index++];
    let n2 = queue[index];
    if(n1.left) {
      queue.push(n1.left);
      index++;
    }
    if(n1.right) {
      queue.push(n1.right);
      index++;
    }
    if(queue.length === count) {
      break;
    }
  }
  queue.reverse();
  for(let i = 0; i < queue.length; i++){
    console.log(queue[i].val);
  }
}
