
const createBinaryTree = require('./create-binary-tree');
const diameter = require('./binary-tree-diameter');

const t1 = createBinaryTree([10,20,30,40,50,2,3,4,5]);

console.log(diameter(t1));