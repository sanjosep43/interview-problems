
const leftView = require('./binary-tree-left-view');
const createTree = require('./create-binary-tree');

const t1 = createTree([1,20,30,40,50,5,4,3,2]);

const results = [];
leftView(t1, results);
console.log(results);