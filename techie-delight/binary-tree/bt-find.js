
function find(node, val) {
  if (!node) {
    return null;
  }
  if (node.val === val) {
    return node;
  }
  return find(node.left, val) || find(node.right, val);
}

module.exports = find;