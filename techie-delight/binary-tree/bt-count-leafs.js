
function countLeafs(node) {
  if (!node.left && !node.right) {
    return 1;
  }
  const c1 = node.left ? countLeafs(node.left) : 0;
  const c2 = node.right ? countLeafs(node.right) : 0;
  return c1 + c2;
}

module.exports = countLeafs;