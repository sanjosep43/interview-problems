

function nodeExistsByVal(root, val) {
  if(!root) {
    return false;
  }
  if(root.val === val){
    return true;
  }
  return nodeExistsByVal(root.left, val) || nodeExistsByVal(root.right, val);
}

module.exports = nodeExistsByVal;
