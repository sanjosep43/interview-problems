

function convertToSumTree(n) {
  if(!n) {
    return 0;
  }
  if(!n.left && !n.right){
    //console.log('leaf:', n.val);
    return n.val;
  }
  console.log('for node:', n.val)
  console.log('-left sum:', convertToSumTree(n.left));
  console.log('-right sum:', convertToSumTree(n.right));
  let sum  = convertToSumTree(n.left) + convertToSumTree(n.right);
  //console.log('sum:', sum, n.val);
  n.sum = n.val + sum;
  //console.log('sum2:',n);
  return sum;
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    //right: l2r
};
let r1 = {
    val: 3,
    //left: r2l,
    //right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

convertToSumTree(root);
console.log(root);
