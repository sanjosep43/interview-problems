
function inorder(n, results) {
  if(!n) {
    return;
  }
  inorder(n.left, results);
  results.push(n.val);
  inorder(n.right, results);
}
function preorder(n, results) {
  if(!n) {
    return;
  }
  results.push(n.val);
  inorder(n.left, results);
  inorder(n.right, results);
}
function postorder(n, results) {
  if(!n) {
    return;
  }
  inorder(n.left, results);
  inorder(n.right, results);
  results.push(n.val);
}



function isSymmetric(n) {
  let left = [];
  postorder(n.left, left);
  let right = [];
  postorder(n.right, right);
  console.log(left);
  console.log(right);
  return;
}

let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 5
}
let r2r = {
    val: 4
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 2,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

console.log(isSymmetric(root));
