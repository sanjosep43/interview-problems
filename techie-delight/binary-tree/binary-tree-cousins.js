
//traversals: depth, BFS
//keep track of parent
//keep track of height

function areCousins(n, n1, n2) {
    //console.log(n, n1, n2);
    let q = [];
    q.push(n);
    while(q.length > 0) {
       let len = q.length;
        while(len--) {
            let node = q.shift();
         let foundN1 = false;
        for(let i = 0; i < q.length; i++) {
            if(q[i] === n1) {
                foundN1 = true;
                break;
            }
        }
        if(foundN1) {
            for(let i = 0; i < q.length; i++) {
                if(q[i] === n2) {
                    return true;
                }
            }
        }
            if(node.left) q.push(node.left);
            if(node.right) q.push(node.right);
        }
    }
    return false;
}
let l2l = {
    val: 4
}
let l2r = {
    val: 5
}
let r2l = {
    val: 6
}
let r2r = {
    val: 7
}
let l1 = {
    val: 2,
    left: l2l,
    right: l2r
};
let r1 = {
    val: 3,
    left: r2l,
    right: r2r
}
let root = {
    val: 1,
    left: l1,
    right: r1
};

console.log(areCousins(root, l1, r1));
console.log(areCousins(root, l2l, r2l));
