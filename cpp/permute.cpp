#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> permute(string str) {
  vector<string> results;
  if(str.empty()) {
    return results;
  }
  /*
  p('abc') = [a + p(bc)] + [b + p(ac)] + [c + p(bc)]
  p(bc) = b + p(c)
  */
  for(int i = 0; str.length(); i++) {
    char c = str[i];
    string first = str.substr(0, i);
    string second = str.substr(i+1);
    vector<string> tempResults = permute(first+second);
    for(int t = 0; t < tempResults.size(); t++) {
      results.push_back(tempResults[t]);
    }
  }
  return results;
}

int main(int argc, char** argv) {
  vector<string> results = permute("abc");
  for(int i = 0; i < results.size(); i++) {
    cout << results[i] << endl;
  }
  return 0;
}