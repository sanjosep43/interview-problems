// Find and return the first duplicate integer in an array in O(n) time and O(1) space.
// Assume there will always be at least one duplicated integer in the array.

function firstDuplicate(input) {
  for(let i = 0; i < input.length; i++) {
    let result = firstDuplicateHelper(input[i], input.slice(i+1));
    // console.log('result=', result);
    if(result !== null) {
      return result;
    }
  }
}

function firstDuplicateHelper(item, input) {
  // console.log('--', item, input, input[0]);
  if(input.length === 0){
    return null;
  }
  if(item === input[0]){
    return item;
  } else {
    return firstDuplicateHelper(item, input.slice(1));
  }
}

console.log(firstDuplicate([1,1]));
console.log(firstDuplicate([1,2,2]));
console.log(firstDuplicate([3,2,1,4,3,2]));