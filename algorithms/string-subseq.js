

function a(strs) {
  //create suffix arrays for each string
  let suffixes = [];
  for (let i = 0; i < strs.length; i++) {
    suffixes.push(suffix(strs[i]));
  }
  console.log(suffixes);
}

function suffix(str) {
  let suffixes = [];
  for (let i = 0; i < str.length; i++) {
    suffixes.push(str.slice(i));
  }
  return suffixes.sort();
}

a(["abc", "dfe", "zij"])