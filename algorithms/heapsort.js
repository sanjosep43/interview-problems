function parent(i) {
  return i/2;
}

function left(i) {
  return i*2;
}

function right(i) {
  return i*2 + 1;
}

function heapSort(a) {

}

function swap(a, x, y) {
  let t = a[x];
  a[x] = a[y];
  a[y] = t;
}

function maxHeapify(a, i) {
  let l = left(i);
  let r = right(i);
  let largestIdx = i;
  if( l <= a.length && a[l] > a[i]) {
    largestIdx = l;
  }
  if( r <= a.length && a[r] > a[largestIdx]) {
    largestIdx = r;
  }
  if( a[largestIdx] != a[i] ){
    swap(a, i, largestIdx);
    heapify(a, largestIdx);
  }
}

function buildMaxHeap(a) {
  for(let i = Math.floor(a.length/2); i > 1; i--){
    maxHeapify(a, i);
  }
}

function heapSort(a) {
  buildMaxHeap(a);
  for(let i = a.length; i > 2; i--) {
    swap(a, 0, i);
    a.heapSize = a.heapSize-1; //TODO
    maxHeapify(a, 0);
  }
}

class Heap extends Array {
  constructor() {
    this.heapSize = this.length;
  }
}

//priority queue