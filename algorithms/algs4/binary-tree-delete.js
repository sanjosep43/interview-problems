
/*
  deleting the min
    1) go left unitl finding a null left link
    2) replace that node by its right link


  1) with no children
  2) with 1 child (replace that child with the node that is deleted)
  3) with 2 children
      find the succesor.  the min child in the right subtree
      delete the min (might have 1 child)
      return the min node
  */