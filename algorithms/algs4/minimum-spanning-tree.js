/*
  given: undirected graph G with positive edge weights
  def: a spanning tree of G is a subgraph T that is both a tree and spanning (inludes all of the vertices)
  goal: find the minimum spanning tree

  cut property
    def: a cut in a graph is a parition of its vertices into 2 sets
    def: a crossing edge connects a vertex in one set with a vertex in the order
    cut property: given an cut, the crossing edge of the min weight is in the MST

  Greedy MST algorithm
    1) start with all edges colored gray
    2) find cut with no black crossing edges; color its min-weight edge black
    3) repeat unit V-1 edges are colored black

    Q: how to find the cut?
*/
class Edge {
  constructor(v, w, weight) {

  }

  either() {

  }

  other(v) {

  }

  compareTo(that){ // Edge

  }
}

class EdgeWeightedGraph {
  constructor(v){

  }
  addEdge(e) {

  }
  adj(v){

  }
}