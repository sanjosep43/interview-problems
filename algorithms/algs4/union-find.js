
class UnionFind {
  constructor(n) {
    this.id = [];
    for(let i = 0; i < n; i++){
      this.id[i] = i;
    }
  }

  find(p, q) {
    return this.id[p] === this.id[q];
  }

  union(p, q) {
    let qid = this.id[q];
    let pid = this.id[p];
    for(let i = 0; i < this.id.length; i++) {
      if(this.id[i] === pid) {
        this.id[i] = qid;
      }
    }
  }
}

module.exports = UnionFind;