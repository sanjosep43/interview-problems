
class QuickUnion {
  constructor(n) {
    this.id = [];
    for(let i = 0; i < n; i++){
      this.id[i] = i;
    }
  }

  root(i) {
    while(i !== this.id[i]) {
      i = this.id[i];
    }
    return i;
  }

  connected(p, q) {
    return this.root(p) === this.root(q);
  }

  union(p, q) {
    let i = this.root(p);
    let j = this.root(q);
    id[i] = j;
  }
}

module.exports = QuickUnion;