
/*
  topological sort:
    ex: precedence scheduling - given a set of tasks to completed with precedence constraints in which order should we schedule the tasks
    digraph model: vertex: task, edge=precedence constraint (only for a directed acyclic graph - DAG)
      0->5
      0->2
      0->1
      3->6
      3->5
      3->4
      5->2
      6->4
      6->0
      3->2
      1->4

      Run DFS
      Return vertices in reverse postorder

      postorder: 4 1 2 5 0 6 3

    0
      2  5  1
        3 . 4
     6
  strong components:
    def: vertices v & w are strongly connected if there is a directed path from v to w and from w to v
*/
class DiGraph {
  constructor() {
    this.V = 0;
    this.E = 0;
    this.adj = []; //set<int>
    this.indegree = []; //int[]
  }

  get V() {
    return this.V;
  }

  get E() {
    return this.E;
  }

  adj(v) {
   return this.adj[v];
  }

  addEdge(v, w) {
    if(!this.adj[v]){
      this.adj[v] = [w];
    } else {
      this.adj[v].push(w);
    }
    if(!this.indegree[w]) {
      this.indegree[w] = 1;
    } else {
      this.indegree[w]++;
    }
    this.E++;
  }

  dfs(){

  }

  bfs() {

  }

  topological_sort(v) {
    /*
      reversePost = []
      marked[v] = true
      for(w of G.adj(v))
        if(!marked[w]) dfs(w)
      reversePost.push(v);
    */
  }

  cycle() {
    //use DFS
  }
}