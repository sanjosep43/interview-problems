
class BiDirectionalSymbolTable {
  constructor() {
    this.keyMap = new Map();
    this.valueMap = new Map();
  }

  put(key, value){
    this.keyMap.set(key, value);
    this.valueMap.set(value, key);
  }

  getByKey(key){
    return this.keyMap.get(key);
  }

  getByValue(value){
    return this.valueMap.get(value);
  }

  delete(key) {
    let v = this.keyMap.get(key);
    this.keyMap.delete(key);
    this.valueMap.delete(v);
  }
}

let sut = new BiDirectionalSymbolTable();

sut.put('10', 'abc');
sut.put('11', 'acb');
sut.put('12', 'bac');

console.log(sut.getByKey('10'));
console.log(sut.getByValue('acb'));
sut.delete('10');
console.log(sut.getByKey('10'));
console.log(sut.getByValue('abc'));
