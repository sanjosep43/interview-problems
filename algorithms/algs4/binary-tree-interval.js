
class BinaryTree {
  constructor() {
    this.root = {};
  }

  insert(interval) {
    let intervals = interval.split('-');
    this.insertHelper(this.root, parseInt(intervals[0]), parseInt(intervals[1]));
  }

  insertHelper(node, lval, rval) {
    if(!Object.keys(node).length) {
      node.lval = lval;
      node.rval = rval;
      node.left = {};
      node.right = {};
      return;
    }
    if(lval < node.lval) {
      this.insertHelper(node.left, lval, rval);
    } else if( lval >= node.lval ) {
      this.insertHelper(node.right, lval, rval);
    }
  }

  find(i) {
    if(!this.root.lval) {
      return false;
    }
    return this.findHelper(this.root.left, i) ||
           this.findHelper(this.root.right, i);
  }

  findHelper(node, i) {
    if(!node){
      return false;
    }
    if(node.lval < i && i <= node.rval) {
      return true;
    }
    return this.findHelper(node.left, i) ||
           this.findHelper(node.right, i);
  }
}

let bt = new BinaryTree();
bt.insert('1643-2033');
bt.insert('5532-7643');
bt.insert('8999-10332');
bt.insert('5666653-5669321');
console.log(bt);
console.log(bt.find(9122));
console.log(bt.find(8122));

module.exports = BinaryTree;