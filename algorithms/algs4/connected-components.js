
/*
  goal: is v connected to w in constant time?

  * a connected component is a maximal set of connected vertices
  * goal: parition vertices into connected components
  * 1) init all vertices v as unmarked
  * 2) for each unmarked vertix v, run DFS to identify all vertices discovered as part of the same component
  *
  v . marked[] cc[]
  0 .   T .     0
  1 .   F
  2
  ..

  * . to visit a vertex v
  *   1) mark vertex v as visited
  *   2) recursivly visit all unmarked vertices adjacent to v
*/

class CC {
  marked = [];
  id = [];
  count = 0;

  constructor(g){
    //graph
    // marked array
    // id array
    // go through each vertex and run dfs
  }
  connected(v, w) {
    //returns bool
  }
  count() {
    //# of connected components
  }
  id(v) {
    //component id for v
  }

  dfs(g, v) {
    // marked[v] = true
    // id[v] = count
    // for g.adj(v)
    // . if !marked then dfs
  }
}