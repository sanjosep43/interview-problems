
class LRUCache {
  constructor() {
    this.list = [];
    this.map = {};
  }

  //inserts the item
  access(item) {
    this.map[item] = true;
    this.list.unshift(item);
  }

  //deletes and returns the least recently accessed
  remove() {
   let item = this.list.shift();
   this.map[item] = false;
   return item;
  }
}