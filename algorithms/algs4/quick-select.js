  // for some j
  // entry a[j] is in place
  // left side of j is smaller
  // right side is larger

  //repeat until i & j pointers cross
  // scan i from l to r so that a[i] < a[lo] . lo is the paritioning element
  // scan j from r to l so that a[j] > a[lo]
  // exchange a[i] and a[j]

  //when pointers cross
  // exchange a[lo] with a[j]
function partition(a, lo, hi){
  let p = a[lo];
  let i = lo + 1;
  let j = hi;
  while(i < j) {
    while(a[i] < a[lo]) ++i;
    while(a[j] > a[lo]) --j;
    swap(a,i,j);
  }
  swap(a, lo, j);
  return j;
}

function swap(a, i, j) {
  [a[i], a[j]] = [a[j], a[i]];
}

function quickSelect(a, k){
    let lo = 0, hi = a.length - 1;
    while(hi > lo) {
      let j = partition(a, lo, hi);
      if(j < k) lo = j + 1;
      if(j > k) hi = j - 1;
      else return a[k];
    }
    return a[k];
}

module.exports = quickSelect;