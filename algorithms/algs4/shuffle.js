

function shuffle(a) {
  for(let i = 0; i < a.length; i++) {
    let r = Math.round(Math.random() * i);
    let t = a[i];
    a[i] = a[r];
    a[r] = t;
  }
}

module.exports = shuffle;