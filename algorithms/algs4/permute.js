/*
  p(123) = (1 + p(23)) +  (2 + p(13)) + (3 + p(12))
  p(23) = (2 + p(3))
  p(13) = (1 + p(3))
  p(12) = (1 + p(2))
  p(3) = 3
  p(2) = 2
  p(1) = 1
*/
function sliceArray(a, x) {
  let r = [];
  for(let i = 0; i < a.length; i++) {
    if(i !== x ) r.push(a[i]);
  }
  return r;
}
function p(a) {
  if(a.length === 1) {
    return a;
  }
  let results = [];
  for(let i = 0; i < a.length; i++) {
    let newA = sliceArray(a, i);
    let tempA = p(newA);
    for(let x = 0; x < tempA.length; x++) {
      results.push([a[i]].concat(tempA[x]));
    }
  }
  return results;
}
console.log(p([1,2,3,4,5,6,7,8,9]));
