const AdjacencyMatrix = require('./adjacency-matrix');

class Graph {
  constructor(vertices) {
    this.vertices = vertices;
    for(let i = 0; i < vertices.length; i++) {
      this.vertices[i].index = i;
    }
    this.adjacencyMatrix = new AdjacencyMatrix();
  }

  createDirectedEdge(fromIdx, toIdx, weight = 1) {
    this.adjacencyMatrix.addDirectedEdge(fromIdx, toIdx, weight);
  }

  createUndirectedEdge(fromIdx, toIdx, weight = 1) {
    this.adjacencyMatrix.addUndirectedEdge(fromIdx, toIdx, weight);
  }

  getAdjacentVertices(sourceIdx) {
    let adjacentIndices = this.adjacencyMatrix.getAdjacencyList(sourceIdx);
    let adjacentVertices = [];
    for(let vertexIdx of adjacentIndices) {
      adjacentVertices.push(this.vertices[vertexIdx]);
    }
    return adjacentVertices;
  }

  getEdgeWeight(v1, v2) {
    return this.adjacencyMatrix.getEdgeWeight(v1.index, v2.index);
  }

  toString() {
    return "TODO";
  }
}

module.exports = Graph;