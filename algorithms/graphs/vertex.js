
class Vertex {
  constructor(data) {
    this.data = data;
    this.index = -1;
  }
  toString() {
    return `${this.index} ${this.data}`;
  }
}

module.exports = Vertex;