
class AdjacencyMatrix {
  constructor(size) {
    this.size = size;
    this.matrix = [];
    for(let i = 0; i < size; i++) {
      this.matrix[i] = [];
    }
  }

  addDirectedEdge(from, to, weight) {
    this.matrix[from][to] = weight;
  }

  addUndirectedEdge(from, to, weight) {
    this.matrix[from][to] = weight;
    this.matrix[to][from] = weight;
  }

  getEdgeWeight(x, y) {
    return this.matrix[x][y];
  }

  getAdjacencyList(index) {
    let adjacencyList = [];
    for(let i = 0; i < this.size; i++) {
      if(this.matrix[index][i] != 0) {
        adjacencyList.push(i);
      }
    }
    return adjacencyList;
  }
}

module.exports = AdjacencyMatrix;