
1. Directed Acyclic Graphs (DAGs)
2. Motivation: program build deps, college class prereq, event scheduling, assembly instructions
3. Kahn's algorithm O(V+E)
  * repeatedly remove nodes without any deps and add them to the topological ordering
  * begin by counting the incoming degree of each nodes and keep in an array
  * keep a queue of 0 degree nodes
  * remove element from queue and update the array of incoming degrees
  * add any new nodes which have degree of zero to queue
  * when element is removed from the queue...add it to the result (topo sort)
  

