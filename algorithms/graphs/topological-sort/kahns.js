function kahns(adjList) {
  const incomingDegrees = [];
  //figure out how many incoming nodes and them to 0
  for (var [key, value] of Object.entries(adjList)) {
    incomingDegrees[key] = 0;
    for (let i = 0; i < value.length; i++) {
      incomingDegrees[value[i]] = 0;
    }
  }
  //set up the incoming degrees
  for (var [key, value] of Object.entries(adjList)) {
    for (let i = 0; i < value.length; i++) {
      incomingDegrees[value[i]]++;
    }
  }
  // console.log('degrees:', incomingDegrees);
  const queue = [];
  const result = [];

  // add all incoming nodes of 0 to queue
  for (let i = 0; i < incomingDegrees.length; i++) {
    if (incomingDegrees[i] === 0) {
      queue.push(i);
    }
  }

  while (queue.length > 0) {
    const element = queue.shift();
    result.push(element);
    const edges = adjList[element];
    for (let e = 0; e < edges.length; e++) {
      incomingDegrees[edges[e]]--;
    }
    console.log('degrees:', incomingDegrees, result);
    // add all incoming nodes of 0 to queue
    for (let i = 0; i < incomingDegrees.length; i++) {
      if (incomingDegrees[i] === 0) {
        // console.log('result:', result);
        if (-1 === result.findIndex((x) => x === i)) {
          // console.log('--pushing', i);
          queue.push(i);
        }
      }
    }
  }

  return result;
}

const adjList1 = {
  0: [1, 3],
  1: [2, 3],
  2: [],
  3: [],
};
console.log(kahns(adjList1));
