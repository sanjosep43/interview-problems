function topologicalSort(jobs, deps) {
  //build adj list
  const adjList = new Map();
  for (let i = 0; i < jobs.length; i++) {
    adjList.set(jobs[i], []);
  }
  for (let i = 0; i < deps.length; i++) {
    const node = deps[i][0];
    const dep = deps[i][1];
    adjList.get(node).push(dep);
  }
  console.log(adjList);
  // init to 0
  const incomingDegrees = [-1];
  for (let i = 0; i < jobs.length; i++) {
    incomingDegrees.push(0);
  }
  //find incoming degree for each node
  for (var [key, value] of adjList.entries()) {
    for (let i = 0; i < value.length; i++) {
      incomingDegrees[value[i]]++;
    }
  }
  console.log('id:', incomingDegrees);
  //queue
  const queue = [];
  for (let i = 0; i < incomingDegrees.length; i++) {
    if (incomingDegrees[i] === 0) {
      queue.push(i);
    }
  }
  console.log('queue:', queue);
  //sort
  const result = [];
  while (queue.length > 0) {
    const node = queue.shift();
    result.push(node);
    console.log('result:', result);
    const neighbors = adjList.get(node);
    for (let i = 0; i < neighbors.length; i++) {
      incomingDegrees[neighbors[i]]--;
    }
    for (let i = 0; i < incomingDegrees.length; i++) {
      if (incomingDegrees[i] === 0 && -1 === result.findIndex((x) => x === i)) {
        queue.push(i);
      }
    }
  }
  return result.length !== jobs.length ? [] : result;
}

console.log(
  topologicalSort(
    [1, 2, 3, 4],
    [
      [1, 2],
      [1, 3],
      [3, 2],
      [4, 2],
      [4, 3],
    ]
  )
);
