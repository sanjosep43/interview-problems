/*
Input: ['baa', 'abcd', 'abca', 'cab', 'cad'];
Output: "bdac"

Input: words = ["caa", "aaa", "aab"]
Output: "cab"
*/

function alienAlphabet(words) {
  words.sort();
  console.log('words:', words);
  const adjList = new Map();
  for (let i = 0; i < words.length; i++) {
    for (let j = 0; j < words[i].length; j++) {
      adjList.set(words[i][j], new Set());
    }
  }
  //build orderings
  for (let i = 0; i < words.length - 1; i++) {
    const w1Len = words[i].length;
    const w2Len = words[i + 1].length;
    const len = w1Len >= w2Len ? w2Len : w1Len;
    const word1 = words[i];
    const word2 = words[i + 1];
    for (let w = 0; w < len; w++) {
      if (word1[w] !== word2[w]) {
        adjList.get(word1[w]).add(word2[w]);
        break;
      }
    }
  }
  console.log(adjList);
  //topological sort
  const incomingDegrees = new Map();
  for (let i = 0; i < words.length; i++) {
    for (let j = 0; j < words[i].length; j++) {
      incomingDegrees.set(words[i][j], 0);
    }
  }
  for (var [k, v] of adjList.entries()) {
    for (const letter of v) {
      incomingDegrees.set(letter, incomingDegrees.get(letter) + 1);
    }
  }
  console.log(incomingDegrees);
  const results = [];
  const queue = [];
  for (var [k, v] of incomingDegrees.entries()) {
    if (v === 0) queue.push(k);
  }
  console.log(queue);
  while (queue.length > 0) {
    const node = queue.pop();
    results.push(node);
    incomingDegrees.delete(node);
    const nodesToDecrement = adjList.get(node);
    console.log('?:', nodesToDecrement, node);
    for (const n of nodesToDecrement) {
      incomingDegrees.set(n, incomingDegrees.get(n) - 1);
    }
    console.log('incoming:', incomingDegrees, 'results:', results);
    for (var [k, v] of incomingDegrees.entries()) {
      if (v === 0 && -1 === results.findIndex((x) => x === k)) queue.push(k);
    }
    console.log('queue:', queue);
  }
  return results;
}

console.log(alienAlphabet(['baa', 'abcd', 'abca', 'cab', 'cad']));
// console.log(alienAlphabet(["caa", "aaa", "aab"]));
