
function binarySearchRecHelper(a, item, s, e) {
  let mid = Math.round((e-s)/2);
  // console.log(s, e, a[mid], item);
  if(a[mid] === item) {
    return true;
  }
  if(s === e){
    return false;
  }
  if(item < a[mid]) {
    return binarySearchRecHelper(a, item, s, mid - 1);
  } else {
    return binarySearchRecHelper(a, item, mid+1, e);
  }
}

function binarySearchRec(a, item) {
  return binarySearchRecHelper(a, item, 0, a.length);
}

function binarySearchIter(a, item) {
  let start = 0;
  let end = a.length-1;
  let mid = Math.floor((end-start)/2);
  while(true) {
    if(item === a[mid]) {
      return true;
    }
    if(item < a[mid]) {
      end = mid - 1;
    } else {
      start = mid + 1
    }
    if(end === 0 || start === end) {
      break;
    }
    mid = Math.floor((end-start)/2);
  }
  return false;
}

// function callstack(i) {
//   console.log(i);
//   callstack(i+1);
// }
// callstack(0);

console.log(binarySearchIter([1,2,3], 7));
console.log(binarySearchIter([1,2,3], 0));
console.log(binarySearchIter([1,2,3], 1));
console.log(binarySearchIter([1,2,3], 2));
console.log(binarySearchIter([1,2,3], 3));
console.log('');
console.log(binarySearchRec([1,2,3], 7));
console.log(binarySearchRec([1,2,3], 0));
console.log(binarySearchRec([1,2,3], 1));
console.log(binarySearchRec([1,2,3], 2));
console.log(binarySearchRec([1,2,3], 3));
