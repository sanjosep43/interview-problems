
function quicksort(a, p, r) {
  if( p < r ) {
    let q = partition(a, p, r);
    quicksort(a, p, q-1);
    quicksort(a, q+1, r);
  }
}

function swap(a, x, y) {
  let t = a[x];
  a[x] = a[y];
  a[y] = t;
}

function partition(a, p, r) {
  let x = a[r];
  let i = p - 1;
  for(let j = p; j < r-1; j++) {
    if(a[j] <= x){
      i = i + 1;
      swap(a, i, j);
    }
  }
  swap(a, i+1, r);
  return i + 1;
}