

function insertionSort(a) {
  for(let i = 1; i < a.length; i++){
    let key = a[i];
    let j = i - 1;
    while(j >= 0 && a[j] > key) {
      a[j+1] = a[j];
      j -= 1;
    }
    a[j + 1] = key;
  }
  return a;
}

console.log(insertionSort([1]));
console.log(insertionSort([2,1]));
console.log(insertionSort([2,3,1]));
console.log(insertionSort([10,20,2,3,1]));
console.log(insertionSort([9,10,20,2,3,1]));