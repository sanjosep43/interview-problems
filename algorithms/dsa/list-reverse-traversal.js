
function run(start) {
  let end = start;
  while(true) {
    if(!end.next) break;
    end = end.next;
  }
  while(true) {
    console.log(end.data);
    if(end === start){
      break;
    }
    let current = start;
    while(true) {
      if(current.next === end) {
        end = current;
        break;
      } else {
        current = current.next;
      }
    }
  }
}


let element1 = {data: 1};
let element2 = {data: 2};
let element3 = {data: 3};
let element4 = {data: 4};

element1.next = element2;
element2.next = element3;
element3.next = element4;
element4.next = null;

run(element1);