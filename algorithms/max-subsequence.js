

function maxSum(v) {
  let maxsofar = 0;
  let maxendinghere = 0;
  for(let i = 0; i < v.length; i++){
    maxendinghere = Math.max(maxendinghere + v[i], 0);
    console.log('1:', i, maxendinghere);
    maxsofar = Math.max(maxsofar, maxendinghere);
    console.log('2:', maxsofar);
  }
  return maxsofar;
}

let result = maxSum([31,-41,59,26,-53,97,93,-23,84]);

console.log(result);