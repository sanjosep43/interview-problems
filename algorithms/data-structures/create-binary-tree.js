
const BinaryTree = require('./binary-tree');

function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

function create(n) {
  let data = [];
  for(let i = 0; i < n; i++) {
    data.push(i);
  }
  shuffle(data);

  let bt = new BinaryTree();
  for(let i = 0; i < data.length; i++) {
    bt.insert(data[i]);
  }
  return bt;
}

let bt1 = create(100000);
console.log(bt1);
console.log(bt1.searchDFSIt(5));
console.log(bt1.searchDFSIt(100001));
console.log(bt1.searchDFSRec(5));
console.log(bt1.searchDFSRec(100001));

module.exports = create;