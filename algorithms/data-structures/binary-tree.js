
class BinaryTree {
  constructor() {
    this.root = undefined;
  }

  insert(data) {
    if(!this.root) {
      this.root = {
        val: data,
        left: {},
        right: {}
      }
      return;
    }
    this.insertHelper(this.root, data);
  }

  insertHelper(node, data) {
    if(Object.keys(node).length === 0) {
      node.val = data;
      node.left = {};
      node.right = {};
      return;
    }
    if(data < node.val) {
      this.insertHelper(node.left, data);
    } else {
      this.insertHelper(node.right, data);
    }
  }

  searchDFSRec(val) {
    return this.searchDFSRecHelper(this.root, val);
  }

  searchDFSRecHelper(node, val) {
    if(Object.keys(node).length === 0){
      return false;
    }
    if(node.val === val){
      return true;
    }
    if(val < node.val) {
      return this.searchDFSRecHelper(node.left, val);
    } else {
      return this.searchDFSRecHelper(node.right, val);
    }
  }

  searchDFSIt(val) {
    let node = this.root;
    while(Object.keys(node).length > 0) {
      if(node.val == val){
        return true;
      }
      if(val < node.val) {
        node = node.left;
      } else {
        node = node.right;
      }
    }
    return false;
  }
}

module.exports = BinaryTree;