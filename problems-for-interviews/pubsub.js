class PubSub {
  constructor() {
    this.events = new Map();
  }

  subscribe(event, callback) {
    if (!this.events.has(event)) {
      this.events.set(event, []);
    }
    this.events.get(event).push(callback);
  }

  publish(event, data) {
    if (this.events.has(event)) {
      this.events.get(event).forEach((callback) => Promise.resolve(callback(data)));
    }
  }

  unsubscribe(event, callback) {
    if (this.events.has(event)) {
      this.events.set(
        event,
        this.events.get(event).filter((cb) => cb !== callback)
      );
    }
  }
}

const delay = (callback) => Promise.resolve(setTimeout(callback, 1000));

const pubsub = new PubSub();

const click1 = (data) => console.log('click1', data);
const click3 = (data) => console.log('click3', data);
const click2 = (data) => delay(() => console.log('click2', data));
pubsub.subscribe('click', click2);
pubsub.subscribe('click', click1);
pubsub.subscribe('click', click3);
pubsub.subscribe('keydown', (data) => console.log('keydown1', data));

pubsub.publish('click', {x: 100, y: 200});
pubsub.unsubscribe('click', click1);
pubsub.publish('click', {x: 100, y: 300});
pubsub.publish('keydown', {key: 'a'});
