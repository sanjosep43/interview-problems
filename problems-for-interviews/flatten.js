//part 1
const example1 = [
  { value: 1, children: [] },
  {
    value: 2, children: [
      { value: 3, children: [] },
      { value: 4, children: [] }
    ]
  },
  { value: 5, children: [] }
];

const flatten = () => {

};

flatten(example1);
/*
  [{value:1}, {value:2}, {value:3}, {value:4}, {value:5}]
*/

const flatten_solution = (input) => {
  let results = [];
  input.forEach(x => {
    results.push({ value: x.value });
    if (x.children.length) {
      results = results.concat(flatten_solution(x.children));
    }
  });
  return results;
};

console.log(flatten_solution(example1));

//part 2

const service = (i) => {
  if (i == 0) {
    return Promise.resolve([{ value: 1, children: [] }]);
  } else if (i === 1) {
    return Promise.resolve([{
      value: 2, children: [
        { value: 3, children: [] },
        { value: 4, children: [] }
      ]
    }]);
  } else if (i === 2) {
    return Promise.resolve([{ value: 5, children: [] }]);
  } else {
    return Promise.reject("invalid index");
  }
};

const getIndexes = (from, to) => {

};

console.log(getIndexes(0, 2));

const getIndexes_Solution = (from, to) => {
  const promises = [];
  for (let i = from; i <= to; i++) {
    promises[i] = service(i);
  }
  return Promise.all(promises).then(results => {
    let output = [];
    for (let i = 0; i < results.length; i++) {
      output = output.concat(flatten_solution(results[i]));
    }
    return output;
  });
}

getIndexes_Solution(0, 2).then(output => console.log(output));