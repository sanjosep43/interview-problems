const map = new Map([
  ['I', 1],
  ['V', 5],
  ['X', 10],
  ['L', 50],
  ['C', 100],
  ['D', 500],
  ['M', 1000],
]);

const romanToInt = (str) => {
  let total = 0;
  for (let i = 0; i < str.length; i++) {
    if (str[i] === 'I' && i <= str.length) {
      if (str[i + 1] === 'V') {
        total += 4;
        i++;
      } else if (str[i + 1] === 'X') {
        total += 9;
        i++;
      } else {
        total += map.get(str[i]);
      }
    } else if (str[i] === 'X' && i <= str.length) {
      if (str[i + 1] === 'L') {
        total += 40;
        i++;
      } else if (str[i + 1] === 'C') {
        total += 90;
        i++;
      } else {
        total += map.get(str[i]);
      }
    } else if (str[i] === 'C' && i <= str.length) {
      if (str[i + 1] === 'D') {
        total += 400;
        i++;
      } else if (str[i + 1] === 'M') {
        total += 900;
        i++;
      } else {
        total += map.get(str[i]);
      }
    } else {
      total += map.get(str[i]);
    }
  }
  return total;
};

console.log(romanToInt('IV'));
console.log(romanToInt('III')); // 3
console.log(romanToInt('LVIII')); //58
console.log(romanToInt('MCMXCIV')); //1994
