function nQueens(n) {
  const board = [];
  for (let y = 0; y < n; y++) {
    board.push([]);
    for (let x = 0; x < n; x++) {
      board[y].push('-');
    }
  }
  solve(n, 0, board);
  return board;
}

function solve(n, col, board) {
  if (n === col) {
    return [true, board];
  }
  for (let x = 0; x < n; x++) {
    if (isValidQueen(board, x, col)) {
      board[col][x] = '*';
      const result = solve(n, col + 1, board);
      if (result[0] === false) {
        board[col][x] = '-';
      }
    }
  }
  return [false, null];
}

function isValidQueen(board, x, y) {
  //check row
  for (let i = 0; i < x; i++) {
    if (board[y][i] === '*') return false;
  }
  //check col
  for (let i = 0; i < y; i++) {
    if (board[i][x] === '*') return false;
  }
  //check left dia
  for (let x1 = 0, y1 = 0; x1 < x && y1 < y; x1++, y1++) {
    if (board[y1][x1] === '*') return false;
  }
  //check right dia
  for (let x1 = x, y1 = y; x1 >= board.length && y1 < 0; x1++, y1--) {
    if (board[y1][x1] === '*') return false;
  }
  return true;
}

console.table(nQueens(4));

/*
  let count = 0;
  let prevX = -1;
  for (let y = 0; y < n; y++) {
    let placedQueen = false;
    let x = prevX === -1 ? 0 : prevX;
    for (; x < n; x++) {
      if (isValidQueen(board, x, y)) {
        board[y][x] = 1;
        prevX = x;
        count += 1;
        if (count === n) {
          return board;
        }
        placedQueen = true;
        break;
      }
    }
    if (!placedQueen) {
      board[y][x] = 0;
      y--;
    }
  }
*/
