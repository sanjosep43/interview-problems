function square(points) {
  if (!points || points.length < 4) {
    return false;
  }
  const start = getStartingPoint(points);
  const numPointsToCheck = points.length;
  let i = 0;
  let current = start;
  while (i < numPointsToCheck) {
    const rightTop = getRightTop(current, points);
    if (!rightTop) {
      current = getNextPoint(current, points, start);
      i++;
      continue;
    }
    const distance = getDistance(current, rightTop);
    const rightBottom = getRightBottom(rightTop, points, distance);
    if (!rightBottom) {
      current = getNextPoint(current, points, start);
      i++;
      continue;
    }
    const leftBottom = getLeftBottom(rightBottom, points, distance);
    if (!leftBottom) {
      return true;
    }
    current = getNextPoint(current, points, start);
    i++;
  }
  return false;
}

function getDistance(point1, point2) {
  // y2 - y1
}

function getStartingPoint(points) {
  // largest x, smallest y
}

function getNextPoint(point, points, start) {
  // go down, then left
}

function getRightTop(point, points) {
  // go right, till next point
}

function getRightBottom(point, points, distance) {
  // go bottom, till next point
}

function getLeftBottom(point, points, distance) {
  // go left till next point
}

console.log(square());
console.log(square([]));
console.log(
  square([
    [0, 0],
    [2, 0],
    [1, 1],
    [0, -1],
    [-1, -1],
    [0, 2],
    [0, 1],
    [1, 0],
  ])
);
