const board = [
  [1, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0, 0],
  [0, 1, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
];

function eightQueens() {
  const placedPositions = [];
  let queenNum = 0;
  while (true) {
    const { emptyCol, emptyRow } = findEmptySquare(board);
    placeQueenAt(board, emptyCol, emptyRow, queenNum + 1);
    placedPositions.push({ col: emptyCol, row: emptyRow });
    if (!isBoardOk(board)) {
      //backtrack - remove last placement.  find a new spot
    }
    if (queenNum === 8) {
      break;
    }
  }
  return board;
}

function findEmptySquare(board) {
  for (let row = 0; i < board.length; row++) {
    for (let col = 0; i < board[0].length; col++) {
      if (board[row][col] === 0) {
        return { emptyCol: col, emptyRow: row };
      }
    }
  }
  return undefined;
}

function placeQueenAt(board, col, row, num) {
  board[row][col] = num;
}

function isBoardOk(board) {
  for (let row = 0; i < board.length; row++) {
    for (let col = 0; i < board[0].length; col++) {
      if (board[row][col] !== 0) {
        if (!checkRow(board, row)) {
          return false;
        }
        if (!checkCol(board, col)) {
          return false;
        }
        if (!checkDiagonal(board, row, col)) {
          return false;
        }
      }
    }
  }
  return true;
}

function checkRow(board, row) {
  let found = false;
  for (let i = 0; i < board.length; i++) {
    if (board[row][i] !== 0) {
      if (found) {
        return false;
      }
      found = true;
    }
  }
  return true;
}

function checkCol(board, col) {
  let found = false;
  for (let i = 0; i < board[0].length; i++) {
    if (board[i][col] !== 0) {
      if (found) {
        return false;
      }
      found = true;
    }
  }
  return true;
}

function getStartLeftTopDiagonal(board, currentRow, currentCol) {
  let row = currentRow;
  let col = currentCol;
  while (true) {
    row -= 1;
    col -= 1;
    if (row === 0 || col === 0) {
      break;
    }
  }
  return { col: col, row: row };
}

function getStartLeftBottomDiagonal(board, currentRow, currentCol) {
  let row = currentRow;
  let col = currentCol;
  while (true) {
    row += 1;
    col -= 1;
    if (row === board.length - 1 || col === 0) {
      break;
    }
  }
  return { col: col, row: row };
}

function checkDiagonal(board, row, col) {
  const leftTop = getStartLeftTopDiagonal(board, row, col);
  let found = false;
  for (let row = leftTop.row; row < board.length; row++) {
    for (let col = leftTop.col; col < board[0].length; col++) {
      if (board[row][col] !== 0) {
        if (found) {
          return false;
        }
        found = true;
      }
    }
  }
  const leftBottom = getStartLeftBottomDiagonal(board, row, col);
  found = false;
  for (let row = leftBottom.row; row >= 0; row--) {
    for (let col = leftBottom.col; col < board[0].length; col++) {
      if (board[row][col] !== 0) {
        if (found) {
          return false;
        }
        found = true;
      }
    }
  }
  return true;
}

console.log(getStartLeftTopDiagonal(board, 3, 5));
console.log(getStartLeftBottomDiagonal(board, 3, 5));