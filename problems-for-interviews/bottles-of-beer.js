function bottlesOfBeer(num) {
  bottlesOfBeerHelper(num, num);
}

function bottlesOfBeerHelper(num, initial) {
  if (num === 0) {
    console.log('No more bottles of beer on the wall, no more bottles of beer.');
    console.log(`Go to the store and buy some more, ${initial} bottles of beer on the wall.`);
    return;
  } else if (num === 1) {
    console.log('1 bottle of beer on the wall, 1 bottle of beer.');
    console.log('Take one down and pass it around, no more bottles of beer on the wall.');
  } else {
    console.log(
      `${num} bottles of beer on the wall, ${num === 1 ? 'bottle' : 'bottles'} of beer on the wall`
    );
    console.log(
      `Take one down and pass it around, ${num - 1} ${
        num - 1 === 1 ? 'bottle' : 'bottles'
      } of beer on the wall.`
    );
  }
  console.log('');
  bottlesOfBeerHelper(num - 1, initial);
}

bottlesOfBeer(5);
