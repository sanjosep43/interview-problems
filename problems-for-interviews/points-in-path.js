
const root1 = {
  val: 10,
  right: {
    val: 18,
    left: {
      val: 15,
      left: { val: 7, left: null, right: null },
      right: { val: 9, left: null, right: null }
    },
    right: {
      val: 11,
      left: null,
      right: null
    }
  },
  left: { val: 3 }
}

const pointsInPath = (node, val1, val2) => {

};

console.log(pointsInPath(root1, 18, 7));

const pointsInPath_solution = (node, val1, val2) => {
  const n1 = helper(node, val1);
  if (!n1) {
    return false;
  }
  return !!helper(n1, val2);
};

const helper = (node, val) => {
  if (!node) {
    return null;
  }
  if (node.val === val) {
    return node;
  }
  return helper(node.left, val) || helper(node.right, val);
}

console.log(pointsInPath_solution(root1, 18, 7));