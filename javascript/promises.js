
var user = (err, res) => {
  if (err) {
    return Promise.reject(err);
  } else {
    return Promise.resolve(res);
  }
}

user(null, 5)
  .then(x => console.log('SUCCESS:', x))
  .catch(x => console.log('ERROR:', x));

user(3, 5)
  .then(x => console.log('SUCCESS:', x))
  .catch(x => console.log('ERROR:', x));

