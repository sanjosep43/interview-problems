"use strict";
var TokenType;
(function (TokenType) {
    TokenType[TokenType["None"] = 0] = "None";
    TokenType[TokenType["StartObject"] = 1] = "StartObject";
    TokenType[TokenType["EndObject"] = 2] = "EndObject";
    TokenType[TokenType["Key"] = 3] = "Key";
    TokenType[TokenType["ValueString"] = 4] = "ValueString";
    TokenType[TokenType["ValueBoolean"] = 5] = "ValueBoolean";
    TokenType[TokenType["ValueNumber"] = 6] = "ValueNumber";
})(TokenType || (TokenType = {}));
let lineNum = 0;
const whitespaceRe = /\s/;
function isWhitespace(buffer, position) {
    if (buffer[position] === '\n') {
        lineNum++;
    }
    return whitespaceRe.test(buffer[position]);
}
function skipWhitespace(buffer, position) {
    let count = 0;
    while (isWhitespace(buffer, position + count)) {
        count++;
    }
    return {
        num: count,
        token: '',
        type: TokenType.None,
    };
}
/*
token list: {}, [] "\w+" \d+ "\w":

then state machine?  in object, in array
*/
const digitRe = /\d/;
function readToken(buffer, position) {
    if (buffer.length === position) {
        return { num: 0, token: '', type: TokenType.None };
    }
    if (buffer[position] === '{') {
        return { num: 1, token: '{', type: TokenType.StartObject };
    }
    else if (buffer[position] === '}') {
        return { num: 1, token: '}', type: TokenType.EndObject };
    }
    else if (buffer[position] === '"') {
        //check if this is key or value string
        let i = 0;
        let key = '';
        for (i = position + 1; i < buffer.length; i++) {
            if (buffer[i] === '"') {
                key = buffer.substring(position + 1, i);
                break;
            }
        }
        const keyLength = key.length + 2;
        const result = skipWhitespace(buffer, position + keyLength);
        const endChar = buffer[position + keyLength + result.num];
        if (key === 'true' || key === 'false') {
            return {
                num: keyLength + result.num + (endChar === ',' ? 1 : 0),
                token: key,
                type: TokenType.ValueBoolean,
            };
        }
        if (buffer[position + keyLength + result.num] === ':') {
            return {
                num: keyLength + result.num + 1,
                token: key,
                type: TokenType.Key,
            };
        }
        else {
            return {
                num: keyLength + result.num + (endChar === ',' ? 1 : 0),
                token: key,
                type: TokenType.ValueString,
            };
        }
    }
    else if (buffer[position].match(digitRe)) {
        let num = '';
        for (let i = position; i < buffer.length; i++) {
            if (buffer[i].match(digitRe)) {
                num += buffer[i];
            }
            else {
                break;
            }
        }
        const result = skipWhitespace(buffer, position + num.length);
        return {
            num: num.length +
                result.num +
                (buffer[position + num.length + result.num] === ',' ? 1 : 0),
            token: num,
            type: TokenType.ValueNumber,
        };
    }
    throw new Error(`invalid token at line:${lineNum} position:${position}`);
}
function parseJson2(buffer, position = 0) {
    lineNum = 0;
    const obj = {};
    let finished = false;
    let result = { num: 0, token: '', type: TokenType.None };
    let currentKey = '';
    while (!finished) {
        result = skipWhitespace(buffer, position);
        position += result.num;
        result = readToken(buffer, position);
        switch (result.type) {
            case TokenType.StartObject:
                position += result.num;
                break;
            case TokenType.EndObject:
                position += result.num;
                break;
            case TokenType.None:
                finished = true;
                break;
            case TokenType.Key:
                position += result.num;
                obj[result.token] = null;
                currentKey = result.token;
                break;
            case TokenType.ValueString:
                position += result.num;
                obj[currentKey] = result.token;
                break;
            case TokenType.ValueBoolean:
                position += result.num;
                obj[currentKey] = result.token === 'true';
                break;
            case TokenType.ValueNumber:
                position += result.num;
                obj[currentKey] = Number(result.token);
                break;
            default:
                throw new Error(`invalid json at line:${lineNum} position:${position}`);
        }
    }
    return obj;
}
// console.log(JSON.stringify(parseJson2('{}'), null, 2));
// console.log(JSON.stringify(parseJson2('   {   }   '), null, 2));
console.log(JSON.stringify(parseJson2(`{   
  "str":"abc",
 "bool"   : "false" ,
 "bool2"   : "true",
 "int"   : 23343,
 "obj":{"key":"value"}
}`), null, 2));
// console.log(JSON.stringify(parseJson('{}'), null, 2));
//# sourceMappingURL=json-parser.js.map