declare enum TokenType {
    None = 0,
    StartObject = 1,
    EndObject = 2,
    Key = 3,
    ValueString = 4,
    ValueBoolean = 5,
    ValueNumber = 6
}
type TokenResult = {
    num: number;
    token: string;
    type: TokenType;
};
declare let lineNum: number;
declare const whitespaceRe: RegExp;
declare function isWhitespace(buffer: string, position: number): boolean;
declare function skipWhitespace(buffer: string, position: number): TokenResult;
declare const digitRe: RegExp;
declare function readToken(buffer: string, position: number): TokenResult;
declare function parseJson2(buffer: string, position?: number): Object;
