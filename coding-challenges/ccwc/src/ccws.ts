import {parseArgs} from 'node:util';
import {readFile} from 'node:fs/promises';
import {open} from 'node:fs/promises';
// import {stdin} from 'node:process';

const {
  values: {charCount, lineCount, wordCount},
  positionals,
} = parseArgs({
  options: {
    charCount: {
      type: 'boolean',
      short: 'c',
    },
    lineCount: {
      type: 'boolean',
      short: 'l',
    },
    wordCount: {
      type: 'boolean',
      short: 'w',
    },
  },
  allowPositionals: true,
});

// open(stdin)
if (positionals.length === 0) {
  console.log('No file provided');
  throw new Error('No file provided');
}

const getFileLength = async (filePath: string) => {
  try {
    const contents = await readFile(filePath, {encoding: 'utf8'});
    return contents.length;
  } catch (err) {
    throw new Error(`${filePath} not found`);
  }
};

const getLinesCount = async function (filePath: string) {
  const file = await open(filePath);
  let lines = 0;
  for await (const line of file.readLines()) {
    lines += 1;
  }
  return lines;
};

const getWordsCount = async function (filePath: string) {
  const file = await open(filePath);
  let words = 0;
  for await (const line of file.readLines()) {
    words += line.split(' ').length;
  }
  return words;
};

let charCountOpt = false,
  lineCountOpt = false,
  wordCountOpt = false;
if (charCount) charCountOpt = true;
if (lineCount) lineCountOpt = true;
if (wordCount) wordCountOpt = true;
if (!charCount && !lineCount && !wordCount) {
  charCountOpt = lineCountOpt = wordCountOpt = true;
}

const runActions = async (filePath: string) => {
  let ccount = -1,
    lcount = -1,
    wcount = -1;
  if (charCountOpt) {
    ccount = await getFileLength(filePath);
  }
  if (lineCountOpt) {
    lcount = await getLinesCount(filePath);
  }
  if (wordCountOpt) {
    wcount = await getWordsCount(filePath);
  }
  return {
    ccount,
    lcount,
    wcount,
  };
};

runActions(positionals[0]).then(({ccount, lcount, wcount}) => {
  const getCountStr = (count: number): string => {
    return count === -1 ? '' : count.toString();
  };

  console.log(
    `  ${getCountStr(wcount)} ${getCountStr(lcount)} ${getCountStr(ccount)} ${
      positionals[0]
    }`
  );
});
