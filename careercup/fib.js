
function fib(n, memo){
  if(n === 0) {
    return 0;
  } else if( n === 1){
    return 1;
  } else if(memo[n]) {
    return memo[n];
  }
  memo[n] = fib(n-1, memo) + fib(n-2, memo);
  return memo[n];
}

console.log(fib(3, []));
console.log(fib(9, []));
console.log(fib(155, []));