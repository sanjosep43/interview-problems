
function collidingAppts(list) {
  if(list.length === 1) {
    return null;
  }
  let allDates = [];
  for(let i = 0; i < list.length; i++) {
    let d = list[i];
    allDates.push({type: 'start', date: d.start});
    allDates.push({type: 'end', date: d.end});
  }
  allDates.sort( (a,b) => {
    if(a.date === b.date) return 0;
    if(a.date < b.date) return -1;
    if(a.date > b.date) return 1;
  } );
  let colliding = [];
  for(let i = 0; i < list.length; i++){
    let elementIdx = allDates.findIndex(x => list[i].start === x.date);
    if(allDates[elementIdx-1].type !== 'end'){
      colliding.push(list[i]);
    }
    elementIdx = allDates.findIndex(x => list[i].end === x.date);
    if(allDates[elementIdx-1].type !== 'start'){
      colliding.push(list[i]);
    }
  }
  return collinding;
}

let date1 = {
  start: new Date('7/8/18'),
  end: new Date('7/18/18')
}
let date2 = {
  start: new Date('7/9/18'),
  end: new Date('7/10/18')
}
let date3 = {
  start: new Date('7/17/18'),
  end: new Date('7/29/18')
}
console.log(collidingAppts([date1]))
