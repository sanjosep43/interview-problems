// https://www.careercup.com/question?id=5100595952222208

function breakApart(str, limit) {
  let numOfBreaks = Math.ceil(str.length/(limit - 5));
  let result = [];
  for(let i = 0; i < numOfBreaks; i++) {
    let start = (limit-5)*i;
    let end = start + limit - 5;
    result.push(str.substring(start, end) +
      '(' + (i+1) + '/' + numOfBreaks + ')');
  }
  return result;
}

console.log(breakApart('Hi Santosh your Uber is arriving now', 15));