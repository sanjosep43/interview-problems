

function reverse(list) {
  if(!list.next) {
    return list;
  }
  if(list.next && !list.next.next) {
  }

  console.log('pre:', list.val)

  let r = reverse(list.next);

  console.log('post:', r ? r.val : undefined);

  return r;
}


let l1 = {
  val: 3,
  next: null
}

let l2 = {
  val: 2,
  next: l1
}

let l3 = {
  val: 1,
  next: l2
}

//console.log(reverse(l3));

function reverse_it(list) {
  let current = list;
  while(current) {
    let temp = current.next;
    current.next.next = current;
    current = temp;
  }
  return current;
}

let third_item = {};
third_item.val = 3;
third_item.next = null;
let second_item = {};
second_item.val = 2;
second_item.next = third_item;
let linked_list = {};
linked_list.val = 1;
linked_list.next = second_item;

console.log(linked_list);
