function swap(a, i, j) {
  let t = a[i];
  a[i] = a[j];
  a[j] = t;
}

function permuteHelper(strArray, l, r, results) {
  if(l == r){
    results.push(parseInt(strArray.join('')));
    return;
  }
  for(let i = l; i <= r; i++) {
    swap(strArray, l, i);
    permuteHelper(strArray, l+1, r, results)
    swap(strArray, l, i);
  }
}

function permute(str) {
  let results = [];
  permuteHelper(str.split(''), 0, str.length-1, results);
  return results;
}

let results = permute("0123456789");
// let results = permute("012");
results = results.sort((a, b) => a - b);
// console.log(results[0])
console.log(results[999999])
console.log(results[1000000])
// console.log(results);

function compareNumbers(a, b) {
  return a - b;
}