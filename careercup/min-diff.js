
class MinDiffList {
  constructor(){
    this.list = [];
    this.min = null;
  }

  add(i) {
    if(this.list.length === 0){
      this.list.push(i);
    } else if (this.list.length === 1){
      this.min = Math.abs(i - this.list[0]);
      this.list.push(i);
    } else {
      this.list.forEach(x => {
        if((x - i) < this.min) {
          this.min = Math.abs(x - i);
        }
      });
      this.list.push(i);
    }
  }

  delete(index) {
    this.list.splice(index, 1);
  }

  minDiff() {
    return this.min;
  }
}

let minDiffList = new MinDiffList();

minDiffList.add(-1);
minDiffList.add(3);
minDiffList.add(4);
minDiffList.add(10);
minDiffList.add(11);
minDiffList.add(11);
console.log(minDiffList.minDiff());