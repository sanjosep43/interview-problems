/*
Write a method that can take in an unordered list of airport
pairs visited during a trip, and return the list in order:

Unordered: ("ITO", "KOA"), ("ANC", "SEA"), ("LGA", "CDG"),
           ("KOA", "LGA"), ("PDX", "ITO"), ("SEA", "PDX")
Ordered:   ("ANC", "SEA"), ("SEA", "PDX"), ("PDX", "ITO"),
           ("ITO", "KOA"), ("KOA", "LGA"), ("LGA", "CDG")

           ordered:
            [{f: ito, s: koa}]
            [{f: pdx, s: ito, ff: false, fs: true},{f: ito, s: koa, ff: true, fs: false}]
            [{f: sea, s: pdx, ff: false, fs: true}, {f: pdx, s: ito, ff: true, fs: true},{f: ito, s: koa, ff: true, fs: false}]
            [{f: sea, s: pdx, ff: true, fs: true}, {f: pdx, s: ito, ff: true, fs: true},{f: ito, s: koa, ff: true, fs: false}]
*/

function order(input) {
  const output = [];
  while(input.length > 0) {
    //console.log('--', output);
    //console.log('-', input)
    if(output.length === 0) {
      output.push(input[0]);
      input.splice(0, 1);
    } else {
      const pair = output[0];
      const i = input.findIndex(x => x.s === pair.f);
      if(i != -1) {
        output.unshift(input[i]);
        input.splice(i, 1);
      } else {
        const end = output[output.length-1];
        const i2 = input.findIndex(x => x.f === end.s);
        if(i2 != -1) {
          output.push(input[i2]);
          input.splice(i2, 1);
        }
      }
    }
  }
  return output;
}

const pairs = [
  { f: "ITO", s: "KOA"},
  { f: "ANC", s: "SEA"},
  { f: "LGA", s: "CDG"},
  { f: "KOA", s: "LGA"},
  { f: "PDX", s: "ITO"},
  { f: "SEA", s: "PDX"}
];

const orderedPairs = order(pairs);
console.log(orderedPairs);