
/*
Given a target sum, populate all subsets, whose sum is equal to the target sum, from an int array.

For example:

Target sum is 15.

An int array is { 1, 3, 4, 5, 6, 15 }.

Then all satisfied subsets whose sum is 15 are as follows:

15 = 1+3+5+6
15 = 4+5+6
15 = 15
*/
function targetSum(a, sum) {
  for(let i = 0; i < a.length; i++) {
    let x = a[i];
    let map = {};
    for(let y = 0; y < a.length; y++){
      if(y !== i) {
        map[a[y]] = sum - x;
      }
    }
  }
}