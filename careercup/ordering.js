// https://www.careercup.com/question?id=5659201272545280

function ordering(input, ordering) {
  let orderingArray = ordering.split('');
  let lastIndex = [];
  for(let i = 0; i < orderingArray.length; i++){
    let currentIdx = -1;
    let from = 0;
    while(true) {
      // console.log(input, orderingArray[i], from);
      currentIdx = input.indexOf(orderingArray[i], from);
      if(currentIdx === -1){
        break;
      }
      from = currentIdx+1;
    }
    lastIndex.push(from);
    if(lastIndex.length !== 1){ 
      if(lastIndex[lastIndex.length-1] < lastIndex[lastIndex.length-2]){
        console.log(lastIndex);
        return false;
      }
    }
  }
  console.log(lastIndex);
  return true;
}


console.log(ordering('hello world!', 'hlo!')); //f
console.log(ordering('hello world!', '!od')); //f
console.log(ordering('hello world!', 'he!')); //t
console.log(ordering('aaaabbbbcccc', 'ac')); //t