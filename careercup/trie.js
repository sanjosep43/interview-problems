
class Trie {
  constructor() {
    this.children = {};
  }

  insert(str) {
    let current = this;
    for(let i = 0; i < str.length; i++){
      if(!current.children[str[i]]) {
        current.children[str[i]] = new Trie();
      }
      current = current.children[str[i]];
    }
  }

  find(str){
    let current = this;
    for(let i = 0; i < str.length; i++){
      if(!current.children[str[i]]) {
        return false;
      }
      current = current.children[str[i]];
    }
    return true;
  }
}

let trie = new Trie();

trie.insert('truck');
trie.insert('trucks');

console.log(trie.find('a'));
console.log(trie.find('truck'));
console.log(trie.find('trucks'));
console.log(trie.find('truckss'));
